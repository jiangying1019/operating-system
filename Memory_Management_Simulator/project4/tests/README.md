# CS441/541 Project 4 Test Suite


## Test Suite
1. Test the command line arguments
ying@ubuntu:~/project4/cs441-f16-project-4$ ./pagefault -f given-tests/level4.txt
Error: Must supply an integer argument between 1 and 7 for the -f option

ying@ubuntu:~/project4/cs441-f16-project-4$ ./pagefault -f 0 given-tests/level4.txt
Error: Must supply an integer argument between 1 and 7 for the -f option

ying@ubuntu:~/project4/cs441-f16-project-4$ ./pagefault -f 50 given-tests/level4.txt
Error: Must supply an integer argument between 1 and 7 for the -f option

ying@ubuntu:~/project4/cs441-f16-project-4$ ./pagefault  given-tests/level4.txt -f
Error: Must supply an integer argument between 1 and 7 for the -f option

ying@ubuntu:~/project4/cs441-f16-project-4$ ./pagefault  given-tests/level4.txt -f 0
Error: Must supply an integer argument between 1 and 7 for the -f option

ying@ubuntu:~/project4/cs441-f16-project-4$ ./pagefault  given-tests/level4.txt -f 70
Error: Must supply an integer argument between 1 and 7 for the -f option
ying@ubuntu:~/project4/cs441-f16-project-4$ 

2.Test commad line arguement and the default -f value. Testing the w,W, R, r
ying@ubuntu:~/project4/cs441-f16-project-4$ ./pagefault tests/test1.txt
Num. Frames : All 
Ref. File : tests/test1.txt
------------------------------
Reference String:
 0:R,  1:W,  3:R,  6:R,  2:W,  4:R,  5:R,  2:W,  5:R,  0:R, 
 3:W,  1:R,  2:R,  5:W,  4:R,  1:R,  0:W,  7:w,  0:R,  1:R, 
 2:r,  0:R,  3:R,  0:R,  4:R,  2:R,  3:R,  0:W,  3:W,  2:R, 
 1:W,  2:R,  0:W,  1:R,  7:R,  0:W,  1:R, 
------------------------------
###################################################
# Frames	Opt.	FIFO	LRU	SC	ESC
#      1	37	37	37	37	37
#      2	25	30	32	30	29
#      3	18	28	25	28	25
#      4	14	24	20	21	24
#      5	11	17	17	18	18
#      6	8	12	11	12	11
#      7	8	11	8	11	8
###################################################
ying@ubuntu:~/project4/cs441-f16-project-4$ ./pagefault tests/test1.txt -f 4
Num. Frames : 4
Ref. File : tests/test1.txt
------------------------------
Reference String:
 0:R,  1:W,  3:R,  6:R,  2:W,  4:R,  5:R,  2:W,  5:R,  0:R, 
 3:W,  1:R,  2:R,  5:W,  4:R,  1:R,  0:W,  7:w,  0:R,  1:R, 
 2:r,  0:R,  3:R,  0:R,  4:R,  2:R,  3:R,  0:W,  3:W,  2:R, 
 1:W,  2:R,  0:W,  1:R,  7:R,  0:W,  1:R, 
------------------------------
###################################################
# Frames	Opt.	FIFO	LRU	SC	ESC
#      4	14	24	20	21	24
###################################################
ying@ubuntu:~/project4/cs441-f16-project-4$ ./pagefault -f  tests/test1.txt
Error: Must supply an integer argument between 1 and 7 for the -f option
ying@ubuntu:~/project4/cs441-f16-project-4$ ./pagefault -f 4 tests/test1.txt
Num. Frames : 4
Ref. File : tests/test1.txt
------------------------------
Reference String:
 0:R,  1:W,  3:R,  6:R,  2:W,  4:R,  5:R,  2:W,  5:R,  0:R, 
 3:W,  1:R,  2:R,  5:W,  4:R,  1:R,  0:W,  7:w,  0:R,  1:R, 
 2:r,  0:R,  3:R,  0:R,  4:R,  2:R,  3:R,  0:W,  3:W,  2:R, 
 1:W,  2:R,  0:W,  1:R,  7:R,  0:W,  1:R, 
------------------------------
###################################################
# Frames	Opt.	FIFO	LRU	SC	ESC
#      4	14	24	20	21	24
###################################################
ying@ubuntu:~/project4/cs441-f16-project-4$ 

3. Testing random 100 integers from 0 t0 9 both inclusive
ying@ubuntu:~/project4/cs441-f16-project-4$ ./pagefault tests/test2.txt 
Num. Frames : All 
Ref. File : tests/test2.txt
------------------------------
Reference String:
 8:R,  2:R,  0:W,  6:W,  0:R,  1:W,  7:W,  6:W,  0:R,  1:W, 
 3:R,  0:W,  0:W,  6:R,  7:W,  7:R,  8:R,  7:R,  6:W,  7:W, 
 2:R,  0:R,  6:R,  3:W,  6:W,  5:R,  7:W,  2:W,  8:R,  7:R, 
 2:R,  1:W,  0:W,  7:W,  0:W,  0:W,  5:R,  1:W,  4:W,  8:W, 
 5:R,  4:W,  8:W,  8:R,  2:R,  3:W,  1:R,  4:R,  6:R,  5:R, 
 1:R,  1:R,  0:W,  8:W,  7:R,  3:R,  1:W,  8:R,  7:R,  5:W, 
 2:R,  3:R,  5:W,  8:R,  2:R,  2:W,  0:R,  4:R,  1:R,  2:R, 
 5:W,  7:W,  6:W,  2:W,  0:W,  6:W,  4:R,  3:R,  0:R,  0:R, 
 8:R,  4:R,  4:R,  2:R,  4:W,  1:R,  1:R,  4:W,  2:R,  1:R, 
 3:R,  7:W,  3:R,  4:W,  0:R,  0:R,  2:W,  6:W,  5:W,  5:R, 

------------------------------
###################################################
# Frames	Opt.	FIFO	LRU	SC	ESC
#      1	89	89	89	89	89
#      2	64	83	81	83	80
#      3	50	67	71	67	67
#      4	37	59	55	59	56
#      5	28	51	49	49	52
#      6	21	40	38	39	36
#      7	16	29	28	27	28
###################################################
ying@ubuntu:~/project4/cs441-f16-project-4$ 

4. Testing 12 randon integers from 0 to 9 both inclusive
ying@ubuntu:~/project4/cs441-f16-project-4$ ./pagefault tests/test3.txt 
Num. Frames : All 
Ref. File : tests/test3.txt
------------------------------
Reference String:
 9:R,  1:W,  5:R,  8:W,  9:W,  2:R,  9:R,  8:R,  8:R,  8:R, 
 8:R,  2:W, 
------------------------------
###################################################
# Frames	Opt.	FIFO	LRU	SC	ESC
#      1	9	9	9	9	9
#      2	6	7	8	7	8
#      3	5	6	6	6	6
#      4	5	6	5	6	5
#      5	5	5	5	5	5
#      6	5	5	5	5	5
#      7	5	5	5	5	5
###################################################
ying@ubuntu:~/project4/cs441-f16-project-4$

5. Testing 16 random integers from 0 to 9 both inclusive 
ying@ubuntu:~/project4/cs441-f16-project-4$ ./pagefault tests/test4.txt 
Num. Frames : All 
Ref. File : tests/test4.txt
------------------------------
Reference String:
 6:W,  0:W,  2:R,  8:R,  0:R,  7:W,  6:W,  1:R,  3:W,  7:W, 
 4:R,  5:R,  6:R,  5:R,  8:R,  8:R, 
------------------------------
###################################################
# Frames	Opt.	FIFO	LRU	SC	ESC
#      1	15	15	15	15	15
#      2	12	14	14	14	14
#      3	10	13	13	13	13
#      4	9	12	12	12	11
#      5	9	11	11	11	11
#      6	9	11	10	11	10
#      7	9	10	10	10	10
###################################################

6. Testing 15 random integers from 0 to 9 both inclusive
ying@ubuntu:~/project4/cs441-f16-project-4$ ./pagefault tests/test5.txt 
Num. Frames : All 
Ref. File : tests/test5.txt
------------------------------
Reference String:
 4:R,  2:R,  1:R,  2:W,  8:R,  5:R,  7:W,  5:W,  6:W,  1:R, 
 9:W,  6:R,  0:R,  8:W,  0:W, 
------------------------------
###################################################
# Frames	Opt.	FIFO	LRU	SC	ESC
#      1	15	15	15	15	15
#      2	11	12	12	12	11
#      3	10	11	11	11	11
#      4	9	11	11	11	11
#      5	9	10	11	10	11
#      6	9	9	10	10	10
#      7	9	9	9	9	9
###################################################
ying@ubuntu:~/project4/cs441-f16-project-4$ 

