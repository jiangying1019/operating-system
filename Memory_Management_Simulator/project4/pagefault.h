
/*
 * 
 *
 * CS 441/541: Page Fault Algorithm Simulator (Project 4)
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>

/******************************
 * Defines
 ******************************/


/******************************
 * Structures
 ******************************/
struct pages{
  int number;
  int dirty;
  char *operation;
  struct pages *next;
};

/******************************
 * Global Variables
 ******************************/


/******************************
 * Function declarations
 ******************************/
//parse arguments
void parse_argus(int args, char **argv);

//create initial reference pages, give each line value to a page
void initial_reference(struct pages **page);

//process line
void process_line(char *line, int *number, int *dirty, char **operation);

//create a page
void create_page(struct pages **page, int number, int dirty, char *operation);

//void run and get result;
void run(int frames);
//initial an array
void initial_array(int array[], int length, int iniValue);

//check have enpty or not if have enpty frames, return the index of this frame
int have_empty_frame(int array[], int length);

//opt
int opt(int frames);

//fifo
int fifo(int frames);

//lru
int lru(int frames);

//sclru
int sclru(int frames);

//esclru
int esclru(int frames);
