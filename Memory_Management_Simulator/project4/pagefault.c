/*
 * 
 *
 * CS 441/541: Page Fault Algorithm Simulator (Project 4)
 */
#include "pagefault.h"
int num_of_frames=0;
int num_of_ref=0;
char *file_name = NULL;
struct pages *page = NULL;

void print_page(){
  int count = 0;
   struct pages *temp = page;
  if(page==NULL);
  else{
    printf("------------------------------\n");
    printf("Reference String:\n");
    while(temp!=NULL){
      printf(" %d:%s, ",temp->number,temp->operation);
      temp=temp->next;
      count++;
      if(count%10==0)
	printf("\n");
      }
    }
  printf("\n------------------------------\n");
}

int main(int argc, char **argv) {
  parse_argus(argc, argv);
  initial_reference(&page);
  if(num_of_frames==0){
    printf("Num. Frames : All \n");
  }else{
    printf("Num. Frames : %d\n", num_of_frames);
  }
  printf("Ref. File : %s\n", file_name);
  print_page();

  run(num_of_frames);

  if(page != NULL){
    struct pages *temp = page;
    for(;temp != NULL;temp = temp->next){
	if(temp->next!=NULL){
	  free(temp->next);
	  temp->next = NULL;
	}
    }
     free(page);
     page = NULL;
  }
    return 0;
}

void parse_argus(int args, char **argv){
  int i = 1;
  if(args==1){
    printf("A file as a parameter is need!\n");
    exit(-1);
  }
  while(i<args){
    if(strcmp(argv[i], "-f")==0){
      if(i+1<args){
	if((num_of_frames=atoi(argv[i+1]))!=0){
	  i = i+2;
	  if(num_of_frames<=0||num_of_frames>7){
	    printf("Error: Must supply an integer argument between 1 and 7 for the -f option\n");
	    exit(-1);
	  }//end if(num_of_frames<=0||num_of_frames>7) 
	}else{
	   printf("Error: Must supply an integer argument between 1 and 7 for the -f option\n");
	    exit(-1);
	}//corresponding to if((num_of_frames=atoi(argv[i+1])!=0)) -f in the middle don't have number
      }else{
	 printf("Error: Must supply an integer argument between 1 and 7 for the -f option\n");
	    exit(-1);
      }//corresponding to if(i+1<args) -f is the last argument doesn't hava number
    }else if(file_name==NULL){
      file_name = argv[i];
      i = i+1;
    }else{
      i = 1+1;
    }
  }//end of while

}
//create a page node
void create_page(struct pages **page, int number, int dirty, char *operation){
  struct pages *pre = *page;
  struct pages *temp;
  if((*page)==NULL){
    (*page)=(struct pages*)malloc(sizeof(struct pages));
    (*page)->number = number;
    (*page)->dirty = dirty;
    (*page)->operation = operation;
    (*page)->next = NULL;
    return;
  }
  temp = pre->next;
  while(temp){
    pre = temp;
    temp = pre->next;
  }

  temp = (struct pages *)malloc(sizeof(struct pages));
  temp->number = number;
  temp->dirty = dirty;
  temp->operation = operation;
  pre->next = temp;
  temp->next = NULL;
}

//read the file and initial the reference string
void initial_reference(struct pages **page){
  FILE *fd = NULL;
  char *fgets_rtn = NULL;
  char line[10];
  int number = 0;
  int dirty = 0;
  char *operation = NULL;
  int i = 1;

  fd = fopen(file_name, "r");
  while(0 == feof(fd)){
   if(i==1){
	 fgets_rtn = fgets( line, 10, fd);
        if( NULL == fgets_rtn ) {
            break;
        }                                                                                                                                                                         
        if( '\n' == line[ strlen(line) -1 ] ) {
            line[ strlen(line) -1 ] = '\0';
        }
	i = i+1;
	num_of_ref = atoi(line);
      }
   else{
	fgets_rtn = fgets( line, 10, fd);
        if( NULL == fgets_rtn ) {
            break;
        }
                                                                                       
        if( '\n' == line[ strlen(line) -1 ] ) {
            line[ strlen(line) -1 ] = '\0';
        }

	process_line(line, &number, &dirty, &operation);
	create_page(page, number, dirty, operation);
      }
  }
  fclose(fd);
}

void process_line(char *line, int *number, int *dirty, char **operation){
  char *copy = strdup(line);
  char *token;
  token = strtok(copy, " ");
  *number = atoi(token);
  *operation = strtok(NULL, " ");
  if(strcmp(*operation, "W")==0||strcmp(*operation, "w")==0){
    *dirty = 1;
  }else{
    *dirty = 0;
  }
}

void run(int frames){
  int mopt=0, mfifo=0, mlru=0, msclru=0, mesclru=0,i=0;
  printf("###################################################\n");
  printf("# Frames\tOpt.\tFIFO\tLRU\tSC\tESC\n");
  if(frames == 0){
    for(i=1;i<8;i++){
      mopt = opt(i);
      mfifo = fifo(i);
      mlru = lru(i);
      msclru = sclru(i);
      mesclru = esclru(i);
      printf("#      %d\t%d\t%d\t%d\t%d\t%d\n",i, mopt, mfifo, mlru, msclru, mesclru);
    }
  }else{
    mopt = opt(frames);
    mfifo = fifo(frames);
    mlru = lru(frames);
    msclru = sclru(frames);
    mesclru = esclru(frames);
    printf("#      %d\t%d\t%d\t%d\t%d\t%d\n",frames, mopt, mfifo, mlru, msclru, mesclru);
  }
   printf("###################################################\n");
  
}

void initial_array(int array[], int length, int iniValue){
  int i=0;
  for(i=0;i<length;i++){
    array[i] = iniValue;
  }
}
//check whether have free frames no not
int have_empty_frame(int array[],int length){
  int i=0;
  for(i=0; i<length; i++){
    if(array[i]==-1)
      return i;
  }
  return -1;
}// -1 means no frame, o.w return the index of the -1 element
//1. If hit, hit=1
//2. If not hit, 1)have free frame:frames[]=current->number;
//               2)no free frame:search forward, find the next position of each frame in memory, if some frames don't appear in the future set future[] to 10000
//3. Deal with the final page
int opt(int frameLength){
  int miss = 0,num=0, i=0, hit=0, j=0, max=0, max_position=0, final=0;
  //  struct pages *pre = page;
  struct pages *current = page;
  struct pages *temp;
  // struct pages *pretemp;
  int frames[frameLength];
  int future[frameLength];
  int flag[frameLength];
  initial_array(frames, frameLength, -1);
  initial_array(future, frameLength, 0);
  initial_array(flag, frameLength, 0);

  while(current->next != NULL){
   
    for(int i=0;i<frameLength;i++){
	if(current->number==frames[i]){
	  hit = 1;
	}
    }//end for tp see whether it is hit or not
  
    if(hit!=1){
		if((num=have_empty_frame(frames, frameLength))!=-1){
		  frames[num] = current->number;
		}//if means some frames are empty
		else{
			for(i=0;i<frameLength;i++){
			  temp = current;
			  j=0;
			  while(temp!=NULL){
			    if(frames[i]==temp->number){
			       future[i] = j;
			       flag[i] = 1;
			       break;
			    }else{
			       j++;
			    }
			    temp = temp->next;
			  }//end while; find the next position of each frames[]
			  temp = current;
			  if(flag[i]==0){
			    future[i]=10000;
			  }
			}//if the frames[i] doesn't appear in the future then set the future[]=10000
			
			max = future[0];
			max_position = 0;
			for(i=0;i<frameLength;i++){
				if(future[i]>max){
				  max = future[i];
				  max_position = i;
				}
			}//find the max value of future, replace the frames[max_position]
			frames[max_position] = current->number;
		}//end else
		miss++;
    }//end if(/miss)
    initial_array(flag,frameLength ,0);
    hit = 0;
    current = current->next;
  }
  
  for(i=0;i<frameLength;i++){
    if(current->number==frames[i]){
      final = 1;
    }
  }
  if(final != 1){
    miss++;
  }//check the final value hit or miss
  return miss;
}
//1. Check hit or not, if hit, all pages "in the memory" times++
//2. If not hit, 1)have free frame: all pages "in the memory" times++; 2).no free frame, find the max times[i], replace i, all times++
// times[ new page]=1
//3. Deal with the final page
int fifo(int frameLength){
  int frames[frameLength];
  int times[frameLength];
  initial_array(frames, frameLength, -1);
  initial_array(times, frameLength, 0);
  struct pages *current = page;
  int i,j, miss=0, hit=0, num=0, max=0, max_position=0,flag = 0;

  while(current->next!=NULL){
    for(i=0;i<frameLength;i++){
      if(frames[i]==current->number){
	hit = 1;
	for(j=0;j<frameLength;j++){
	  if(frames[j]!=-1)
	  times[j] = times[j]+1;
	}
      }//end if
    }//end for i
    //if doesn't hit, check have empty frame or not, if don't have empty frame, then replace one frame
    //if replace on frame, the repalces one times should be 1, other times should ++. 
    if(hit != 1){
      if((num = have_empty_frame(frames, frameLength))!=-1){
	frames[num] = current->number;
	for(j=0;j<frameLength;j++){
	  if(frames[j]!=-1)
	  times[j] = times[j]+1;
	}//if still have enpty frame then times++;
      }else{//if no empty frames, then victim times is 1 other frames times ++
	max = times[0];
	max_position = 0;
	for(j=0;j<frameLength;j++){
	  if(times[j]>max){
	    max = times[j];
	    max_position = j;
	  }
	}
	frames[max_position] = current->number;
	times[max_position] = 1;
	for(j=0;j<frameLength;j++){
	  if(j!=max_position)
	  times[j] = times[j]+1;
	}
      }
      miss++;
    }
    hit = 0;
    current = current->next;    
  }
  //check the last one is hit or miss
  for(i=0;i<frameLength;i++){
    if(current->number == frames[i]){
      flag = 1;
    }
  }
  if(flag!=1){
    miss++;
  }  
  return miss;
}
//1. Check hit or not. If hit,hit count[i]=0,other count[]++(two kinds of suitations: have free frame and no free frame)
//2. If not hit, 1)have free frame:count[0-num]++; 2) no free frame:find the max count[i], i is the victim, the count[new page]=0, others count[]++
//3. Deal with the final page, if miss, miss++
// You made a mistake, you can't just from the current palce count frameLength and get that value.
int lru(int frameLength){
  int frames[frameLength];
  int count[frameLength];
  initial_array(frames, frameLength, -1);
  initial_array(count, frameLength, 0);
  struct pages *current = page;
  int i=0,j=0,hit=0, miss=0, num=0,flag=0,max=0,max_position=0;
  while(current->next!=NULL){
    for(i=0;i<frameLength;i++){
      if(current->number==frames[i]){
	hit = 1;
	if((num=have_empty_frame(frames, frameLength))!=-1){
	  for(j=0;j<num;j++){
	    count[j] = count[j]+1;
	  }
	}else{
	  for(j=0;j<frameLength;j++){
	    count[j] = count[j]+1;
	  }
	}
	count[i] = 0;
       }
    }
    if(hit != 1){
      if((num=have_empty_frame(frames, frameLength))!=-1){
	frames[num] = current->number;
	count[num]=0;
	for(i=0;i<num;i++){
	  count[i] = count[i]+1;
	}
      }else{
	max = 0;
	max_position=0;
	for(i=0;i<frameLength;i++){
	  if(count[i]>max){
	    max = count[i];
	    max_position=i;
	  }
	}//find the max count[] value
	for(i=0;i<frameLength;i++){
	  count[i] = count[i]+1;
	}// all count++
	frames[max_position]=current->number;
	count[max_position]=0;
      }
      miss++;
    }
    hit = 0;  
    current = current->next;
  }
  //check the last one is hit or miss
  for(i=0;i<frameLength;i++){
    if(current->number==frames[i])
      flag = 1;
  }
  if(flag != 1){
    miss++;
  }
  return miss;
}

//the pointer is pointing to the next palce to replace
//1. Check hit or not, if hit, ref bit is 1
//2. If not hit, 1)have free frame: ref bit is 1, pointer move; 2)no free frame: if the ref is 1, set the ref to 0,find the first ref=0, replace
//3. Deal with the final page
int sclru(int frameLength){
  int frames[frameLength];
  int refarray[frameLength];
  struct pages *current = page;
  initial_array(frames, frameLength, -1);
  initial_array(refarray, frameLength, -1);
  int i, hit=0, miss=0, count=0, pointer=0, flag=0, num=0;

  while(current->next!=NULL){
    for(i=0;i<frameLength;i++){
      if(frames[i]==current->number){
	hit = 1;
	refarray[i]=1;
      }
    }//hit
    //not hit
    if(hit!=1){
      //have free frame
      if((num=have_empty_frame(frames, frameLength))!=-1){
	frames[num]=current->number;
	refarray[num]=1;
	count++;
	pointer = count%frameLength;
      }else{//no frame
	while(refarray[pointer]==1){
	  refarray[pointer]=0;
	  count++;
	  pointer = count%frameLength;
	}//find which ref =0 if ref =1 set ref=0
	frames[pointer] = current->number;
	refarray[pointer]=1;
	count++;
	pointer = count%frameLength;
      }
      miss++;
    }
    hit = 0;
    current=current->next;
  }
 //check the last one is hit or miss
    for(i=0;i<frameLength;i++){
    if(current->number==frames[i])
      flag = 1;
    }
  if(flag != 1){
    miss++;
    }
  return miss;
}
//You made a mistake, the page in the memory modify bit is 1, then the page is hit and modify bit is 0, the modify bit should be 1, not the new modify bit 0.
//1. If hit, set the modify bit and ref bit
//2. If not hit, 1)have free frame, set modify bit, ref bit, pointer move;
//               2)no free frame: first loop find the (0,0), if not find-->second loop find the (0,1) at the same time set ref=0, after the second loop, if not find continue from first loop 
//3. Deal with the final page
int esclru(int frameLength){
  int frames[frameLength];
  int ref[frameLength];
  int modify[frameLength];
  initial_array(frames, frameLength, -1);
  initial_array(ref, frameLength, -1);
  initial_array(modify, frameLength, -1);
  struct pages *current = page;
  int i=0, hit=0, miss=0, num=0, flag=0, pointer=0,count=0, find = 0;

  while(current->next!=NULL){
    for(i=0;i<frameLength;i++){
      if(current->number==frames[i]){
	hit = 1;
	ref[i]=1;
	if(modify[i]==0)
	  modify[i]=current->dirty;
	else
	  modify[i]=1;	  
      }
    }

    if(hit != 1){
      if((num=have_empty_frame(frames, frameLength))!=-1){
	frames[num] = current->number;
	ref[num] = 1;
	modify[num] = current->dirty;
	count++;
	pointer = count%frameLength;
      }else{
	while(find==0){
	  for(i=0;i<frameLength;i++){
	    if((ref[pointer]==0)&&(modify[pointer]==0)){
	       find = 1;
	      frames[pointer]=current->number;
	      ref[pointer]=1;
	      modify[pointer] = current->dirty;
	      count++;
	      pointer = count%frameLength;
	      break;
	    }//if ref and modify both not zero
	    else{
	      count++;
	      pointer = count%frameLength;
	    }
	  }//end FIRST for loop. find any frames ref and modify is zero
	  if(find!=1){
	    for(i=0;i<frameLength;i++){
	      if(ref[pointer]==0&&modify[pointer]==1){
		find=1;
		frames[pointer]=current->number;
		ref[pointer]=1;
		modify[pointer]=current->dirty;
		count++;
		pointer = count%frameLength;
		break;
	      }else{
		ref[pointer]=0;
		count++;
		pointer = count%frameLength;
	      }//edn else
	    }//end the SECOND for loop
	  }//end if not find. at this time find ref=0, modify=1 and set ref=0
	}//end while find==0. At this time find the victim
      }//end else
      miss++;
    }//end if(hit!=1)
    find=0;
    hit = 0;
    current = current->next;
  }
  
 //check the last one is hit or miss
    for(i=0;i<frameLength;i++){
    if(current->number==frames[i])
      flag = 1;
    }
    if(flag != 1){
    miss++;
    }
  
  return miss;
}
