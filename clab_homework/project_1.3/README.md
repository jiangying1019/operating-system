## Operating System homework clab_1.2

This project is about: Command Line Processing.
This program will be processing the command line arguments stored in the argv array.
The argc variable provides a count of the number of arguments stored in the argv array. 


How to use this program:
$make
$./program3
##Input the string after '$$ '.
##To end this program enter "quit" or CTRL-D.



Test cases:
ying@ubuntu:~/clab1/clab_hwk_1.3$ make
make: Nothing to be done for 'all'.
ying@ubuntu:~/clab1/clab_hwk_1.3$ ./program3
$$ This is a test. Testing 123!!!
   1)"This is a test. Testing 123!!!"
  30/   5/   3/  16/   2/   4
$$ 12345 67890
   2)"12345 67890"
  11/   1/  10/   0/   0/   0
$$ 
   3)""
   0/   0/   0/   0/   0/   0
$$    
   4)"   "
   3/   3/   0/   0/   0/   0
$$ quit
#--------------------------------
# Number of strings: 4
# Number of spaces: 9
# Number of digits: 13
# Number of lowercase: 16
# Number of uppercase: 2
#--------------------------------

ying@ubuntu:~/clab1/clab_hwk_1.3$ ./program3
$$ This is another test! Test ctrl-D        
   1)"This is another test! Test ctrl-D"
  33/   5/   0/  23/   3/   2
$$ .......    
   2)"......."
   7/   0/   0/   0/   0/   7
$$ #--------------------------------
# Number of strings: 2
# Number of spaces: 5
# Number of digits: 0
# Number of lowercase: 23
# Number of uppercase: 3
#--------------------------------



Author: Jiang Ying
Date: 12/09/2016