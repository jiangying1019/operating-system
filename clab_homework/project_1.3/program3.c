/*Author: Jiang  Ying
 *Date:9/11/2016
 *This is homework project3. Processing standard input.
 **/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define LINELEN 1024

int dissect_string(char *str, int *num_space, int *num_digit, int *num_lower, int *num_upper, int *num_other)
{
    int i=0; 
    while(str[i]!='\0')
        {
            if(isspace(str[i]))
                *num_space =*num_space+1;
            else if(isdigit(str[i]))
                *num_digit = *num_digit+1;
            else if(islower(str[i]))
                *num_lower = *num_lower+1;
            else if(isupper(str[i]))
                *num_upper = *num_upper+1;
            else
                *num_other = *num_other+1;
            i++;
        }
    return i;
}

int main(int argc, char **argv)
{
    int num_line=0;/*to count the lines user input*/

    int num_space, num_digit, num_lower, num_upper, num_other, len;
    int space=0,digit=0,lower=0,upper=0,other=0;
  
    char *str=NULL;
    char buffer[LINELEN];

    printf("$$ ");
    str = fgets(buffer, LINELEN, stdin );
    if(str != NULL){
        if('\n' == buffer[strlen(buffer)-1]){
            buffer[ strlen(buffer)-1] = '\0';
        }
    }

    while(strcmp(str,"quit")!=0){
        num_space=0; num_digit=0; num_lower=0; num_upper=0; num_other=0; len=0;
    
        num_line++;
        printf("%4d)\"%s\"\n",num_line,str);
        len=dissect_string(buffer,&num_space,&num_digit,&num_lower,&num_upper,&num_other);
        printf("%4d/%4d/%4d/%4d/%4d/%4d\n",len,num_space,num_digit,num_lower,num_upper,num_other);

        space=num_space+space;/*The sum of all spaces, digitals,uppercase, lowercase, and other characters */
        digit=num_digit+digit;
        lower=num_lower+lower;
        upper=num_upper+upper;
        other=num_other+other;

        printf("$$ ");
        fflush(NULL);
        str = fgets(buffer, LINELEN, stdin );
        if(str != NULL){
            if('\n' == buffer[strlen(buffer)-1]){
                buffer[ strlen(buffer)-1] = '\0';
            }
        } /*change the last enter character '\n' to '\0' otherwise the ENTER can be counted as a character */
        else
            break;/*deal with the ctrl D*/
    }

    printf("#--------------------------------\n");/*print the result**/
    printf("# Number of strings: %d\n",num_line);
    printf("# Number of spaces: %d\n",space);
    printf("# Number of digits: %d\n",digit);
    printf("# Number of lowercase: %d\n",lower);
    printf("# Number of uppercase: %d\n",upper);
    printf("#--------------------------------\n");
    return 0;
}
