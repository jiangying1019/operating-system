## Operating System homework clab_1.2

Description:
This program will be processing the command line arguments stored in the argv array.
The following command line argument(s) can occur in any order on the command line.
If a required argument is not provided or if there are insuﬃcient arguments to an option then a helpful error must be displayed, and the program must exit with a negative exit status.
If additional command line options are provided then an error must be displayed and the program must exit with a negative exit status.
The error message must start with the words “Error:”, clearly identify the problem including the token that caused the problem, if appropriate.
If it is a valid command line set then your program will display a summary of the command line options before exiting – matching the formatting seen in the examples.
• Required: -n # Number of iterations. Parameter must be a positive integer greater than 0.
• Optional: -d Enable debugging output in the main program. Default: Disabled.
• Optional: -stage NAME Run only the stage identiﬁed by the string NAME. Default: Run all stages



How to use the program:
$ make
$ ./project1 (This area are testing arguments.)




Test examples:
ying@ubuntu:~/clab1/clab_hwk_1.2$ make
gcc project1.c -o project1 -g -O0 -Wall
ying@ubuntu:~/clab1/clab_hwk_1.2$ ./project1  -d -stage FOO
Error: -n option required to be specified.

ying@ubuntu:~/clab1/clab_hwk_1.2$ ./project1 -n  -d -stage FOO
ERROR: not enouth arguments to the -n option.

ying@ubuntu:~/clab1/clab_hwk_1.2$ ./project1 -n -20 -d -stage
ERROR:-n option requires a positive integer greater than 0!

ying@ubuntu:~/clab1/clab_hwk_1.2$ ./project1 -n 20 -d -stage
#----------------------------------
#Iterations: 20
#Debugging: Enabled
#Stage(s): All (Default)
#--------------------------------------

ying@ubuntu:~/clab1/clab_hwk_1.2$ ./project1  -stage FOO  FFF  -d -n 20 
ERROR: Unknown Option: FFF!

ying@ubuntu:~/clab1/clab_hwk_1.2$ ./project1  -stage FOO  -g  -d -n 20 
ERROR: Unknown Option: -g!

ying@ubuntu:~/clab1/clab_hwk_1.2$ ./project1 -n 20 -d -stage FOO
#----------------------------------
#Iterations: 20
#Debugging: Enabled
#Stage(s): FOO
#--------------------------------------

ying@ubuntu:~/clab1/clab_hwk_1.2$ ./project1  -stage FOO -n 20
#----------------------------------
#Iterations: 20
#Debugging: Disabled (Default)
#Stage(s): FOO
#--------------------------------------

ying@ubuntu:~/clab1/clab_hwk_1.2$ ./project1  -stage FOO  -d -n 20
#----------------------------------
#Iterations: 20
#Debugging: Enabled
#Stage(s): FOO
#--------------------------------------


Author: Jiang Ying
Date: 12/09/2016
