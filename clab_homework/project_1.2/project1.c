/*Author: Jiang Ying
 *Date: 09/11/2016
 *Homework program 2. Write a C program to process command line arguments
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

       /*To test the parameter of '-n' is a posotive number or not.*/
int negetivenum(char argv[])
{
    int c;
    int len=strlen(argv);
    if(argv[0]=='-')
        {
            for(c=1;c<len;c++)
                {
                    /* if(!(((argv[c]-'0')>=0)&&((argv[c]-'9')<=0)))*/
                    if(!isdigit(argv[c]))
                        return -2 ;/*not a negetive number may be an argument*/
                }
            return 2;/*negrtive number*/
        }
    else
        {
            for(c=0;c<len;c++)
                {
                    /*if(!(((argv[c]-'0')>=0)&&((argv[c]-'9')<=0)))*/
                    if(!isdigit(argv[c]))
                        return -1 ;/*any other characters*/
                }
            return 1;/*posirive number*/
        }
    return 0;
}


/*To test the parameter of '-stage' is capitial or not*/
int name(char argv[])
{
    int b;
    int len=strlen(argv);
  
    if(argv[0]!='-')
        { for(b=0;b<len;b++)
                {  
                    if(!isupper(argv[b]))
                        return 3;/*is not capital*/
                }
        }
    else
        return 2;/*It is a parameter*/

    return 1;/*A valid capital*/
}

/*Print the result*/
void printresult(int nflag, int dflag, int sflag, char *number, char *name)
{
    if(nflag&&number!=NULL)
        {printf("#----------------------------------\n");
            printf("#Iterations: %s\n",number);
        }
    else
        {printf("Error: -n option required to be specified.\n");
            exit(-1);
        }

    if(dflag)
        {printf("#Debugging: Enabled\n");}
    else{printf("#Debugging: Disabled (Default)\n");}

    if(sflag)
        {  if(name!=NULL)
                printf("#Stage(s): %s\n",name); 
            else{printf("#Stage(s): All (Default)\n");}
        }
    else printf("#Stage(s): All (Default)\n");
    printf("#--------------------------------------\n");
}


int main(int argc, char *argv[])
{
    int i=1;
    int nflag=0;/*To mark the statue of '-n' argument*/
    int dflag=0;/*To mark the statue of '-d' argument*/
    int sflag=0;/*To mark the statue of '-stage' argumen*/
    char *sname=NULL;/*To record the value of NAME*/
    char *number=NULL;/*To record the value of Iteration*/

    while(i<argc){
        if(strcmp(argv[i],"-n")==0)
            {  nflag=1;
                if(((i+1)<argc))
                    { 
                        if(negetivenum(argv[i+1])==2)
                            { printf("ERROR:-n option requires a positive integer greater than 0!\n");
                                exit(-1);
                            }
                        if(negetivenum(argv[i+1])==-2)
                            {
                                printf("ERROR: not enouth arguments to the -n option.\n");
                                exit(-1);
                            }
                        if(negetivenum(argv[i+1])==1)
                            { i=i+1;
                                number=strdup(argv[i]);
                            }
                    }
                else
                    {
                        printf("Error: not enough arguments to the -n option.\n");
                        exit(-1);
                    }

            }/*process argument -n*/

        else if(strcmp(argv[i], "-d")==0)
            {
                dflag=1;
            }/*process -d*/

        else if(strcmp(argv[i],"-stage")==0)
            {
                sflag=1;
                if(((i+1)<argc))
                    {   
                           
                        if(name(argv[i+1])==3)
                            {printf("ERROR:NAME is capital\n"); exit(-1);}
                        if(name(argv[i+1])==1)
                            { 
                                i=i+1;
                                sname=strdup(argv[i]);
                            }
                    }
            }/*process argument '-stage'*/
    
        else
            { printf("ERROR: Unknown Option: %s!\n", argv[i]);
                exit(-1);
            }
        i=i+1;
    }
    printresult(nflag,dflag,sflag,number,sname);
    return 0;
}
