/*
 * Josh Hursey, Samantha Foley, Jiang Ying
 *
 * CS441/541: Project 2
 *
 */
#include "mysh.h"
#include <stdlib.h>
#include <stdio.h>


struct node *head = NULL;

int main(int argc, char * argv[]) {
    int ret = 0;

    /*
     * Parse Command line arguments to check if this is an interactive or batch
     * mode run.
     */
    if( 0 != (ret = parse_args_main(argc, argv)) ) {
        fprintf(stderr, "Error: Invalid command line!\n");
        return -1;
    }

    /*
     * If in batch mode then process all batch files
     */
    if( TRUE == is_batch) {
        if( TRUE == is_debug ) {
            printf("Batch Mode!\n");
        }

        if( 0 != (ret = batch_mode()) ) {
            fprintf(stderr, "Error: Batch mode returned a failure!\n");
        }
    }
    /*
     * Otherwise proceed in interactive mode
     */
    else if( FALSE == is_batch ) {
        if( TRUE == is_debug ) {
            printf("Interactive Mode!\n");
        }

        if( 0 != (ret = interactive_mode()) ) {
            fprintf(stderr, "Error: Interactive mode returned a failure!\n");
        }
    }
    /*
     * This should never happen, but otherwise unknown mode
     */
    else {
        fprintf(stderr, "Error: Unknown execution mode!\n");
        return -1;
    }
    
    /*
     * Display counts
     */
    printf("-------------------------------\n");
    printf("Total number of jobs               = %d\n", total_jobs);
    printf("Total number of jobs in history    = %d\n", total_history);
    printf("Total number of jobs in background = %d\n", total_jobs_bg);

    /*
     * Cleanup
     */
    if( NULL != batch_files) {
        free(batch_files);
        batch_files = NULL;
        num_batch_files = 0;
    }
   
     if(head!=NULL){
      struct node *temp = head;
      for(;temp!=NULL;temp = temp->next){
	if(temp->data!=NULL){
	  free(temp->data);
	  temp->data = NULL;
	}
	if(temp->next!=NULL){
	  free(temp->next);
	  temp->next = NULL;
	}
      }
      free(head);
      head = NULL;
      }
     int i=0;
     if(jobs_bg!=NULL){
       for(i=0;i<total_jobs_bg;i++){
	 if(jobs_bg[i].full_command!=NULL){
	   free(jobs_bg[i].full_command);
	   jobs_bg[i].full_command=NULL;
	 }
       }
       free(jobs_bg);
       jobs_bg=NULL;
     }
    return 0;
}

int parse_args_main(int argc, char **argv)
{
    int i;

    /*
     * If no command line arguments were passed then this is an interactive
     * mode run.
     */
    if( 1 >= argc ) {
        is_batch = FALSE;
        return 0;
    }

    /*
     * If command line arguments were supplied then this is batch mode.
     */
    is_batch = TRUE;
    num_batch_files = argc - 1;
    batch_files = (char **) malloc(sizeof(char *) * num_batch_files);
    if( NULL == batch_files ) {
        fprintf(stderr, "Error: Failed to allocate memory! Critical failure on %d!", __LINE__);
        exit(-1);
    }

    for( i = 1; i < argc; ++i ) {
        batch_files[i-1] = strdup(argv[i]);
    }

    return 0;
}

int batch_mode(void)
{
    int i;
    int ret;
    char * command = NULL;
    char * cmd_rtn = NULL;
    FILE *batch_fd = NULL;
   
    command = (char *) malloc(sizeof(char) * (MAX_COMMAND_LINE+1));
    if( NULL == command ) {
        fprintf(stderr, "Error: Failed to allocate memory! Critical failure on %d!", __LINE__);
        exit(-1);
    }

    for(i = 0; i < num_batch_files; ++i) {
        if( TRUE == is_debug ) {
            printf("-------------------------------\n");
            printf("Processing Batch file %2d of %2d = [%s]\n", i, num_batch_files, batch_files[i]);
            printf("-------------------------------\n");
        }

        /*
         * Open the batch file
         * If there was an error then print a message and move on to the next file.
         */
        if( NULL == (batch_fd = fopen(batch_files[i], "r")) ) {
            fprintf(stderr, "Error: Unable to open the Batch File [%s]\n", batch_files[i]);
            continue;
        }

        /*
         * Read one line at a time.
         */
        while( FALSE == exiting && 0 == feof(batch_fd) ) {

            /* Read one line */
            command[0] = '\0';
            if( NULL == (cmd_rtn = fgets(command, MAX_COMMAND_LINE, batch_fd)) ) {
                break;
            }

            /* Strip off the newline */
            if( '\n' == command[strlen(command)-1] ) {
              command[strlen(command)-1] = '\0';
            }

            /*
             * Parse and execute the command
             */
            if( 0 != (ret = split_parse_and_run(command)) ) {
                fprintf(stderr, "Error: Unable to run the command \"%s\"\n", command);
            }
        }

        /*
         * Close the batch file
         */
        fclose(batch_fd);
    }

    /*
     * Cleanup
     */
    if( NULL != command ) {
        free(command);
        command = NULL;
    }

    return 0;
}

int interactive_mode(void)
{
    int ret;
    char * command = NULL;
    char * cmd_rtn = NULL;
   
    /*
     * Display the prompt and wait for input
     */
    command = (char *) malloc(sizeof(char) * (MAX_COMMAND_LINE+1));
    if( NULL == command ) {
        fprintf(stderr, "Error: Failed to allocate memory! Critical failure on %d!", __LINE__);
        exit(-1);
    }

    do {
        /*
         * Print the prompt
         */
        printf("%s", PROMPT);
        fflush(NULL);

        /*
         * Read stdin, break out of loop if Ctrl-D
         */
        command[0] = '\0';
        if( NULL == (cmd_rtn = fgets(command, MAX_COMMAND_LINE, stdin)) ) {
            printf("\n");
            fflush(NULL);
            break;
        }

        /* Strip off the newline */
        if( '\n' == command[strlen(command)-1] ) {
	  command[strlen(command)-1] = '\0';
	}

        /*
         * Parse and execute the command
         */
        if( 0 != (ret = split_parse_and_run(command)) ) {
            fprintf(stderr, "Error: Unable to run the command \"%s\"\n", command);
            /* This is not critical, just try the next command */
        }

    } while( NULL != cmd_rtn && FALSE == exiting);

    /*
     * Cleanup
     */

    if( NULL != command ) {
        free(command);
        command = NULL;
    }

    return 0;
}

int split_parse_and_run(char * command)
{
    int ret, i, j;
    int    num_jobs = 0;
    job_t *loc_jobs = NULL;
    char * dup_command = NULL;
    int bg_idx;
    int valid = FALSE;
   
    /*
     * Sanity check
     */
    if( NULL == command ) {
        return 0;
    }

    /*
     * Check for multiple sequential or background operations on the same
     * command line.
     */
    /* Make a duplicate of command so we can sort out a mix of ';' and '&' later */
    dup_command = strdup(command);

    
    /*Add commands to history list*/
    insert(&head, command);
    
    /******************************
     * Split the command into individual jobs
     ******************************/
    /* Just get some space for the function to hold onto */
    loc_jobs = (job_t*)malloc(sizeof(job_t) * 1);
    if( NULL == loc_jobs ) {
        fprintf(stderr, "Error: Failed to allocate memory! Critical failure on %d!", __LINE__);
        exit(-1);
    }
    split_input_into_jobs(command, &num_jobs, &loc_jobs);

    /*
     * For each job, check for background or foreground
     * Walk the command string looking for ';' and '&' to identify each job as either
     * sequential or background
     */
    bg_idx = 0;
    valid = FALSE;
    for(i = 0; i < strlen(dup_command); ++i ) {
        /* Sequential separator */
        if( dup_command[i] == ';' ) {
            if( TRUE == valid ) {
                loc_jobs[bg_idx].is_background = FALSE;
                ++bg_idx;
                valid = FALSE;
            }
            else {
                fprintf(stderr, "Error: syntax error near unexpected token ';'\n");
            }
        }
        /* Background separator */
        else if( dup_command[i] == '&' ) {
            if( TRUE == valid ) {
                loc_jobs[bg_idx].is_background = TRUE;
                ++bg_idx;
                valid = FALSE;
            }
            else {
                fprintf(stderr, "Error: syntax error near unexpected token '&'\n");
            }
        }
        /*
         * Look for valid characters. So we can print an error if the user
         * types: date ; ; date
         */
        else if( dup_command[i] != ' ' ) {
            valid = TRUE;
        }
    }

    /*
     * For each job, parse and execute it
     */
    for( i = 0; i < num_jobs; ++i ) {
        if( 0 != (ret = parse_and_run( &loc_jobs[i] )) ) {
            fprintf(stderr, "Error: The following job failed! [%s]\n", loc_jobs[i].full_command);
        }
    }

    /*
     * Cleanup
     */
    if( NULL != loc_jobs ) {
        /* Cleanup struct fields */
        for( i = 0; i < num_jobs; ++i ) {
            if( NULL != loc_jobs[i].full_command ) {
                free( loc_jobs[i].full_command );
                loc_jobs[i].full_command = NULL;
            }

            if( NULL != loc_jobs[i].argv ) {
                for( j = 0; j < loc_jobs[i].argc; ++j ) {
                    if( NULL != loc_jobs[i].argv[j] ) {
                        free( loc_jobs[i].argv[j] );
                        loc_jobs[i].argv[j] = NULL;
                    }
                }
                free( loc_jobs[i].argv );
                loc_jobs[i].argv = NULL;
            }

            loc_jobs[i].argc = 0;

            if( NULL != loc_jobs[i].binary ) {
                free( loc_jobs[i].binary );
                loc_jobs[i].binary = NULL;
            }
        }
        /* Free the array */
        free(loc_jobs);
        loc_jobs = NULL;
    }
  if( NULL != dup_command ) {
        free(dup_command);
        dup_command = NULL;
  }
    return 0;
}

int parse_and_run(job_t * loc_job)
{
    int ret;

    /*
     * Sanity check
     */
    if( NULL == loc_job ||
        NULL == loc_job->full_command ) {
        return 0;
    }

    /*
     * No command specified
     */
    if(0 >= strlen( loc_job->full_command ) ) {
        return 0;
    }

    if( TRUE == is_debug ) {
        printf("        \"%s\"\n", loc_job->full_command );
    }

    ++total_history;

    /******************************
     * Parse the string into the binary, and argv
     ******************************/
    split_job_into_args(loc_job);

    /* Check if the command was just spaces */
    if( 0 >= loc_job->argc ) {
        return 0;
    }

    /* Grab the binary from the list of arguments */
    if( 0 < loc_job->argc ) {
        loc_job->binary = strdup(loc_job->argv[0]);
    }


    /******************************
     * Check for built-in commands:
     * - jobs
     * - exit
     * - history
     * - wait
     * - fg
     ******************************/
    if( 0 == strncmp("exit", loc_job->binary, strlen(loc_job->binary)) ) {
        if( 0 != (ret = builtin_exit() ) ) {
            fprintf(stderr, "Error: exit command failed!\n");
        }
    }
    else if( 0 == strncmp("jobs", loc_job->binary, strlen(loc_job->binary)) ) {
        if( 0 != (ret = builtin_jobs() ) ) {
            fprintf(stderr, "Error: jobs command failed!\n");
        }
    }
    else if( 0 == strncmp("history", loc_job->binary, strlen(loc_job->binary)) ) {
        if( 0 != (ret = builtin_history() ) ) {
            fprintf(stderr, "Error: history command failed!\n");
        }
    }
    else if( 0 == strncmp("wait", loc_job->binary, strlen(loc_job->binary)) ) {
        if( 0 != (ret = builtin_wait() ) ) {
            fprintf(stderr, "Error: wait command failed!\n");
        }
    }
    else if( 0 == strncmp("fg", loc_job->binary, strlen(loc_job->binary)) ) {
        if( 0 != (ret = builtin_fg(loc_job) ) ) {
            fprintf(stderr, "Error: fg command failed!\n");
        }

    }
    /*
     * Launch the job
     */
    else {
        if( 0 != (ret = launch_job(loc_job)) ) {
            fprintf(stderr, "Error: Unable to launch the job! \"%s\"\n", loc_job->binary);
        }
    }

    return 0;
}
//When the argument is binary, then it can be executed by linux shell
//fork and execvp system calls can help finish this job
int launch_job(job_t * loc_job)
{
    int i;
    pid_t c_pid = 0;
    int status;
  
    /*
     * Display the job
     */
    /*  printf("Job %2d%c: \"%s\" ",
           total_jobs_display_ctr + 1,
           (TRUE == loc_job->is_background ? '*' : ' '),
           loc_job->binary);//print command*/
  

    /*    for( i = 1; i < loc_job->argc; ++i ) {
      printf(" [%s]", loc_job->argv[i]);//print argues
        fflush(NULL);
    }
    printf("\n");
    fflush(NULL);*/

    /*
     * Launch the job in either the foreground or background
     */

    /*
     * Some accounting
     */
    
    ++total_jobs;
    ++total_jobs_display_ctr;
    if( TRUE == loc_job->is_background ) {
        ++total_jobs_bg;
	
	c_pid = fork();
	if(c_pid<0){
	  printf("Error: fork() failed!!!\n");
	  exit(-1);
	}
        else if(c_pid==0){
	    if(execvp(loc_job->argv[0], loc_job->argv)==-1)
	      printf("Error! execvp() failed!!!\n");	   
	}
	else{
	   waitpid(c_pid,&status,WNOHANG);
	}
	
	/*
 	 * add the job into the back jobs list
	 */	
	jobs_bg=(job_bg *)realloc((jobs_bg),sizeof(job_bg) * total_jobs_bg);
	if(NULL == (jobs_bg)){
		fprintf(stderr,"Error: Failed to allocate memory! Critical failure on %d!",__LINE__);
		exit(-1);
	}
    //record the information of the background process
    /* pid is the pid of a process
    *  index is the number of job
    *  status is the status of process 1 means running 0 means returned
    *  full_command is the command of a process
    */
	jobs_bg[total_jobs_bg-1].pid=c_pid;
   	jobs_bg[total_jobs_bg-1].index=total_jobs;
	jobs_bg[total_jobs_bg-1].status=1;
	jobs_bg[total_jobs_bg-1].full_command=strdup(loc_job->full_command);
       	//jobs_bg[total_jobs_bg-1].argv=loc_job->argv;
	jobs_bg[total_jobs_bg-1].argc=loc_job->argc;
    //get the arguments of one command
	 for( i = 1; i < loc_job->argc; ++i ) {
                jobs_bg[total_jobs_bg-1].full_command=(char *)realloc((jobs_bg[total_jobs_bg-1].full_command),sizeof(char) * (strlen(jobs_bg[total_jobs_bg-1].full_command)
                                                                +strlen(loc_job->argv[i])+2));
                jobs_bg[total_jobs_bg-1].full_command=strcat(strcat(jobs_bg[total_jobs_bg-1].full_command," "),loc_job->argv[i]);
        }
    }else{
        c_pid = fork();

	if(c_pid <0){
	  printf("Error: fork() failed!!!\n");
	  exit(-1);
	}
	else if(c_pid == 0){
	  if(execvp(loc_job->argv[0], loc_job->argv) == -1){
	    fprintf(stderr,"Error! Exec failed!!!\n");
	    exit(-1);
	  }
    }
    else{
      waitpid(c_pid, &status, 0);
      }
    }
    fflush(NULL);
    return 0;
}

//check the status of process to see whether a process is finished or not
int builtin_exit(void)
{
   exiting = TRUE;
 
   int i=0;
   int rtn = 1,status;
    while(i<total_jobs_bg){
      rtn = waitpid(jobs_bg[i].pid,&status, WNOHANG);
    if(jobs_bg[i].status==1){
      if(rtn ==0){//no process exit
	printf("Waiting for job [%-4d] %s done\n",jobs_bg[i].index+1, jobs_bg[i].full_command);
	waitpid(jobs_bg[i].pid,&status,0);
      } 
    }
    i++;
   }
   
    ++total_jobs_display_ctr;
    fflush(NULL);
    
    return 0;
}

int builtin_jobs(void)
{
      
	/*
 	 *  the implements of jobs function:
 	 *     search the jobs_bk list to find if the full command of the back
 	 *     groud running jobs
 	 */

  int i=0;
	int rtn=1;
	int status;

	while(i<total_jobs_bg){
		rtn=(int)waitpid((pid_t)jobs_bg[i].pid,&status,WNOHANG);
		
		if(jobs_bg[i].status==1){	
			if(rtn==0){
				jobs_bg[i].status=1;
			}else{
				jobs_bg[i].status=0;
			}
				printf("[%-4d]\t%s\t%s\t\n",jobs_bg[i].index,(jobs_bg[i].status==0)?"Done":"Running",
					jobs_bg[i].full_command);
			
		}
		i++;						
	}
    ++total_jobs_display_ctr;
    fflush(NULL);
    return 0;
}


//To print all history command lines
//struct node is a linklist to store history shell commands that executed before
int builtin_history(void)
{ int i=0;
  struct node *temp = head;
  if(head==NULL);
  else{
      if( '\n' == temp->data[strlen(temp->data)-1] ){
      temp->data[strlen(temp->data)-1] = '\0';
      }
    while(temp!=NULL){
      if(temp->data[0]!='\0'){
         i++;
	 printf("%d\t%s\n",i, temp->data);
       }
      temp=temp->next;
      }
    }
    ++total_jobs_display_ctr;
    fflush(NULL);

    return 0;
}

int builtin_wait(void)
{
  // printf("Job %2dx: \"wait\"\n", total_jobs_display_ctr + 1);
    ++total_jobs_display_ctr;
    fflush(NULL);

    	int i=0;
	int ret = 1;
	int status = 0;
    //go throuth all processes to check whether they returned or not
    //if not return, wait for all the processes return
	while(i<total_jobs_bg){
	   ret = waitpid(jobs_bg[i].pid, &status,WNOHANG);
	   if(jobs_bg[i].status==1){
	      if(ret==0){
		printf("Waiting [%-4d] %s\n", jobs_bg[i].index, jobs_bg[i].full_command);
		waitpid(jobs_bg[i].pid, &status, 0);
	      }
	 }
       	i++;						
	}

    return 0;
}



int builtin_fg(job_t *loc_job)
{
  int latest_job = -1, argu_value = -1;
  pid_t pid;
  int i=0,j;
  int ret = 1;
  int status = 0;
  
  if(loc_job->argc > 2){
     printf("fg takes zero or one arguments.!\n");
     return 0;
  }
  if(loc_job->argc==2){
     argu_value = atoi(loc_job->argv[1]);
     // if input argument is bigger than total_jobs_bg  or agru_value is invalid
     if(argu_value > total_jobs_bg||argu_value==0){
       printf("Error: inavlid argument\n");
       return 0;
     }//if it is a valid command, wait for the specific pid.
     if((ret = waitpid(jobs_bg[argu_value-1].pid,&status,WNOHANG))==0){
       printf("%s\n", jobs_bg[argu_value-1].full_command);
       waitpid(jobs_bg[argu_value-1].pid, &status,0);
    }else{
       printf("Error: Job is alredy done.\n");
      return 0;
   }
  
  }
  //if there is no argument
  if(loc_job->argc==1){
	while(i<total_jobs_bg){
	   ret = waitpid(jobs_bg[i].pid, &status,WNOHANG);
	   if(jobs_bg[i].status==1){
	      if(ret==0){
		j = i;
		pid = jobs_bg[i].pid;
		latest_job = jobs_bg[i].index;
	      }
	 }
       	i++;						
	}//get the latest job

	//if all jobs are done print error
	if(latest_job==-1){
	  printf("Error: no jobs in the background!!!\n");
	  return 0;
	}else{
	  printf("%s\n", jobs_bg[j].full_command);
	  waitpid(pid,&status,0);
	}//else wait for the latest job
  }
  
    ++total_jobs_display_ctr;
    fflush(NULL);

    return 0;
}

