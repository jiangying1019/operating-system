CS441/541 Project 2

Ying Jiang

Date: 10/06/2016



Description:
This project will implement a command line interpreter or shell. The shell will be similar to, but much simpler than, the one in a Unix environment. 


How to build the software
1.Batch mode:
ying@ubuntu:~/mysh_new/mysh$ make
make: Nothing to be done for 'all'.
ying@ubuntu:~/mysh_new/mysh$ ./mysh tests/file1.txt

2.Interactive mode:
ying@ubuntu:~/mysh_new/mysh$ make
make: Nothing to be done for 'all'.
ying@ubuntu:~/mysh_new/mysh$ ./mysh
mysh$ 




How to use the software
In batch mode, input paremeter is a file.
For example:ying@ubuntu:~/mysh_new/mysh$ ./mysh tests/file1.txt
Then the output will show on screen.

In interactive mode, no input when execute the software.
For example:ying@ubuntu:~/mysh_new/mysh$ ./mysh
    	    mysh$
"mysh$" is a prompt, after that you should input shell commands.



How the software was tested
Batch mode:
test/file4.txt
output:
ying@ubuntu:~/mysh_new/mysh$ ./mysh tests/file4.txt
given-tests  history.txt  Makefile  mysh  mysh.c  mysh.c~  mysh.h  mysh.h~  output.txt	#README.md#  README.md	README.md~  support.c  support.c~  support.h  support.h~  support.o  tests
[2   ]	Running	sleep 5	
[3   ]	Running	sleep 7	
sleep 5
[2   ]	Done	sleep 5	
[3   ]	Running	sleep 7	
sleep 3
Error: Job is alredy done.
Error: Job is alredy done.
Error: inavlid argument
Waiting [4   ] sleep 5
[3   ]	Done	sleep 7	
[4   ]	Done	sleep 5	
[5   ]	Done	sleep 3	
1	ls
2	sleep 5&
3	sleep 7&
4	jobs
5	fg 1
6	jobs
7	sleep 5&
8	sleep 3&
9	fg
10	fg 1
11	fg 2
12	fg 6
13	wait
14	jobs
15	jobs
16	history
-------------------------------
Total number of jobs               = 5
Total number of jobs in history    = 17
Total number of jobs in background = 4
ying@ubuntu:~/mysh_new/mysh$ ./mysh tests/file3.txt



test/file3.txt
output:
ying@ubuntu:~/mysh_new/mysh$ ./mysh tests/file3.txt
Thu Oct  6 21:08:49 CDT 2016
Thu Oct  6 21:08:49 CDT 2016
Thu Oct  6 21:08:49 CDT 2016
[4   ]	Running	sleep 5	
[5   ]	Running	 sleep 6	
[6   ]	Running	 sleep 7	
sleep 5
 sleep 7
[4   ]	Done	sleep 5	
[5   ]	Done	 sleep 6	
[6   ]	Done	 sleep 7	
1	date
2	date
3	date
4	sleep 5& sleep 6& sleep 7&
5	jobs
6	fg 1
7	fg
8	jobs
9	jobs
10	wait
11	sleep 5&
12	history
Waiting for job [7   ] sleep 5 done
-------------------------------
Total number of jobs               = 7
Total number of jobs in history    = 15
Total number of jobs in background = 4
ying@ubuntu:~/mysh_new/mysh$ 





test/file2.txt
output:
ying@ubuntu:~/mysh_new/mysh$ ./mysh tests/file2.txt
Thu Oct  6 21:11:17 CDT 2016
[2   ]	Running	sleep 30	
[3   ]	Running	 sleep 2	
Thu Oct  6 21:11:17 CDT 2016
 sleep 2
given-tests  history.txt  Makefile  mysh  mysh.c  mysh.c~  mysh.h  mysh.h~  output.txt	#README.md#  README.md	README.md~  support.c  support.c~  support.h  support.h~  support.o  tests
1	date
2	sleep 30& sleep 2&
3	jobs
4	date
5	fg 2
6	ls
7	history
Error! Exec failed!!!
1	date
2	sleep 30& sleep 2&
3	jobs
4	date
5	fg 2
6	ls
7	history
8	wrong input
9	history
date
pwd
ls tests
sleep 4&;
sleep 5&
jobs
fg 1
date
sleep 30&
wait
sleep 30&
history
exit
1	date
2	sleep 30& sleep 2&
3	jobs
4	date
5	fg 2
6	ls
7	history
8	wrong input
9	history
10	cat tests/file1.txt
11	history
Waiting for job [2   ] sleep 30 done
-------------------------------
Total number of jobs               = 7
Total number of jobs in history    = 13
Total number of jobs in background = 2
ying@ubuntu:~/mysh_new/mysh$ 





test/file1.txt
output:
ying@ubuntu:~/mysh_new/mysh$ ./mysh tests/file1.txt
Thu Oct  6 21:12:29 CDT 2016
/home/ying/mysh_new/mysh
file1.txt  file1.txt~  file2.txt  file2.txt~  file3.txt  file3.txt~  file4.txt	file4.txt~  file.txt
Error: syntax error near unexpected token ';'
[4   ]	Running	sleep 4	
[5   ]	Running	sleep 5	
sleep 4
Thu Oct  6 21:12:33 CDT 2016
Waiting [5   ] sleep 5
Waiting [7   ] sleep 30
1	date
2	pwd
3	ls tests
4	sleep 4&;
5	sleep 5&
6	jobs
7	fg 1
8	date
9	sleep 30&
10	wait
11	sleep 30&
12	history
Waiting for job [8   ] sleep 30 done
-------------------------------
Total number of jobs               = 8
Total number of jobs in history    = 13
Total number of jobs in background = 4
ying@ubuntu:~/mysh_new/mysh$ 


Interactive mode:
ying@ubuntu:~/mysh_new/mysh$ ./mysh 
mysh$ sleep 3& sleep 4& sleep 5&
mysh$ wait
Waiting [1   ] sleep 3
Waiting [2   ]  sleep 4
Waiting [3   ]  sleep 5
mysh$ fg 2   
Error: Job is alredy done.
mysh$ sleep 1& sleep 4& sleep 5& 
mysh$ jobs
[1   ]	Done	sleep 3	
[2   ]	Done	 sleep 4	
[3   ]	Done	 sleep 5	
[4   ]	Done	sleep 1	
[5   ]	Running	 sleep 4	
[6   ]	Running	 sleep 5	
mysh$ fg 6
Error: Job is alredy done.
mysh$ jobs
[5   ]	Done	 sleep 4	
[6   ]	Done	 sleep 5	
mysh$ jobs
mysh$ sleep 6&
mysh$ exit
Waiting for job [8   ] sleep 6 done
-------------------------------
Total number of jobs               = 7
Total number of jobs in history    = 14
Total number of jobs in background = 7
ying@ubuntu:~/mysh_new/mysh$ 




## Known bugs and problem areas

Not implement redirect function
