/*
 * Run with
 *   ./traverse Testing 123 Test 76
 */
#include <stdio.h>
#include <string.h>
#include <ctype.h>

int main(int argc, char **argv) {
    char str[7] = "Hello!";
    int i, s;

    // print the string and its length
    // strlen uses the null character to determine the length
    printf("Str = <%s>, Len = %d\n", str, (int)strlen(str));

    // what happens if we add a null character?
    str[2] = '\0';
    printf("Str = <%s>, Len = %d\n", str, (int)strlen(str));

    // what happens if we start printing from just beyond that character?
    printf("Str = <%s>, Len = %d\n", &(str[3]), (int)strlen( &(str[3]) ));

    for(i = 1; i < argc; i++ ) {
        printf("Checking \"%s\"\n", argv[i]);

        for(s = 0; s < strlen( argv[i] ); s++ ) {
            if( isdigit( argv[i][s] ) ) {
                printf("\tDig  = \"%c\"\n", argv[i][s]);
            }
            else {
                printf("\tChar = \"%c\"\n", argv[i][s]);
            }
        }
    }

    return 0;
}
