#include <stdio.h>
#include <string.h>

int fill_str(char *str, char c);

int main(int argc, char **argv) {
    char str[9] = "Hello!";

    printf("Before: %s\n", str);
    fill_str( str, 'a' );
    printf("After : %s\n", str);

    return 0;
}

int fill_str(char *str, char c) {
    int i;

    if( NULL == str ) {
        return -1;
    }

    for( i = 0; i < strlen(str); ++i ) {
        str[i] = c;
    }

    return 0;
}
