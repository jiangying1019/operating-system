#include <stdio.h>
#include <string.h>

int dup_substr(char *src, char **dest, int pos);

int main(int argc, char **argv) {
    char *src  = "Magic";
    char *dest = "foo";

    printf("Before: (%10s) (%10s)\n", src, dest);
    dup_substr( src, &dest, 1 );
    printf("After : (%10s) (%10s)\n", src, dest);

    return 0;
}

int dup_substr(char *src, char **dest, int pos) {
    int s_pos;
    int d_pos;

    if( NULL == src || NULL == (*dest) ) {
        return -1;
    }

    s_pos = pos;
    d_pos = 0;

    (*dest) = strdup(src);
    while( s_pos < strlen(src) ) {
        
        (*dest)[d_pos] = src[s_pos];
        //*dest[d_pos] = src[s_pos];
        d_pos++;
        s_pos++;
    }
    (*dest)[d_pos] = '\0';
    printf("DEBUG: %s\n", (*dest) );

    return 0;
}
