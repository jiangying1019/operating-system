/*
 * Josh Hursey
 *
 * CS 441/541: Reduction Example with Pthreads
 */
#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>

/* Number of threads to create */
#define NUM_THREADS 10
/* Max Buffer size */
#define BUFFER_SIZE 250

/*
 * Buffer of random integers
 * Shared between all threads in the process
 */
int buffer[BUFFER_SIZE];

/*
 * Subtotals from each thread
 * Shared between all threads in the process
 * Each thread should only write into its index into this array!
 */
long subtotals[NUM_THREADS];

/*
 * Thread Start Function
 * Add a segment of numbers in the buffer
 * Store the result in the segment of 'subtotals' for this thread.
 *  *threadid : integer artificial thread ID
 */
void *add_numbers(void *threadid);

int main(int argc, char **argv) {
    pthread_t threads[NUM_THREADS];
    int rc, i;
    void *status = NULL;
    long sum;

    /*
     * Initialize data
     */
    srandom(time(NULL));
    for(i = 0; i < BUFFER_SIZE; ++i) {
        buffer[i] = random() % 1024;
    }
    for(i = 0; i < NUM_THREADS; ++i) {
        subtotals[i] = 0;
    }

    /*
     * Create Threads
     */
    for(i = 0; i < NUM_THREADS; ++i) {
        printf("In Main(): Creating thread %d\n", i);
        rc = pthread_create(&threads[i],
                            NULL,
                            add_numbers,
                            (void *)(intptr_t)i);
        if(0 != rc ) {
            fprintf(stderr, "Error: Cannot Create thread\n");
            exit(-1);
        }
    }

    /*
     * Join Threads
     */
    for(i = 0; i < NUM_THREADS; ++i ) {
        rc = pthread_join(threads[i], &status);
        if( 0 != rc ) {
            fprintf(stderr, "Error: Cannot Join Thread %d\n", i);
            exit(-1);
        }
        //printf("Joined thread %d status set to %ld\n", i, (long)status);
    }

    /*
     * Calculate the total
     */
    sum = 0;
    for(i = 0; i < NUM_THREADS; ++i) {
        sum += subtotals[i];
    }

    printf("The sum of %5d random numbers is    : %ld\n",
           BUFFER_SIZE, sum);
    printf("The average of %5d random numbers is: %6.2f\n",
           BUFFER_SIZE, (sum/(double)BUFFER_SIZE));

    /*
     * Exit
     */
    pthread_exit(NULL);

    return 0;
}

void *add_numbers(void *threadid) {
    int tid = (intptr_t)threadid;
    int start_idx, end_idx;
    int idx;

    printf("Thread %d: Checking in...\n", tid);

    // Define my region of the buffer
    start_idx = 0; // TODO
    end_idx   = 0; // TODO

    // Calculate the subtotal
    // TODO
    idx = 0;

    // All done
    pthread_exit((void*)(intptr_t)tid);
}
