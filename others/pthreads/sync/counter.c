/*
 * Josh Hursey
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <errno.h>
#include <pthread.h>

#define UPPER_BOUND 1000

/*
 * Thread functions to test prefix and postfix
 */
void *inc_counter_post(void *threadid);
void *inc_counter_pre(void *threadid);

// Shared counter
int counter = 0;

int main(int argc, char * argv[]) {
    int ret, i;
    pthread_t *threads = NULL;
    int num_threads = 1;

    /*
     * Parse args
     */
    if( argc < 2 ) {
        fprintf(stderr, "Error: Usage: %s NUMTHREADS\n", argv[0]);
        exit(-1);
    }

    num_threads = strtol(argv[1], NULL, 10);
    if( num_threads < 1 || num_threads > 2048 ) {
        fprintf(stderr, "Error: Number of threads must be between [1,2048]\n");
        exit(-2);
    }
    
    threads = (pthread_t*)malloc(sizeof(pthread_t) * num_threads);
    if( NULL == threads ) {
        fprintf(stderr, "Error: Out-of-memory\n");
        exit(-3);
    }

    /********************* Post-fix operator test ***************/
    counter = 0;

    /*
     * Create thread(s)
     */
    for(i = 0; i < num_threads; ++i) {
        ret = pthread_create(&threads[i],
                             NULL,
                             inc_counter_post,
                             (void *)(intptr_t)i);
        if(0 != ret ) {
            fprintf(stderr, "Error: Cannot Create thread\n");
            exit(-1);
        }
    }

    /*
     * Join Thread(s)
     */
    for(i = 0; i < num_threads; ++i ) {
        ret = pthread_join(threads[i], NULL);
        if( 0 != ret ) {
            fprintf(stderr, "Error: Cannot Join Thread %d\n", i);
            exit(-1);
        }
    }

    printf("Post) Expected %10d : Actual %10d : Difference %10d\n",
           (num_threads * UPPER_BOUND),
           counter,
           ((num_threads * UPPER_BOUND) - counter) );


    /********************* Pre-fix operator test ***************/
    counter = 0;
    /*
     * Create thread(s)
     */
    for(i = 0; i < num_threads; ++i) {
        ret = pthread_create(&threads[i],
                             NULL,
                             inc_counter_pre,
                             (void *)(intptr_t)i);
        if(0 != ret ) {
            fprintf(stderr, "Error: Cannot Create thread\n");
            exit(-1);
        }
    }

    /*
     * Join Thread(s)
     */
    for(i = 0; i < num_threads; ++i ) {
        ret = pthread_join(threads[i], NULL);
        if( 0 != ret ) {
            fprintf(stderr, "Error: Cannot Join Thread %d\n", i);
            exit(-1);
        }
    }
    
    printf("Pre ) Expected %10d : Actual %10d : Difference %10d\n",
           (num_threads * UPPER_BOUND),
           counter,
           ((num_threads * UPPER_BOUND) - counter) );

    /*
     * Cleanup
     */
    if( NULL != threads ) {
        free(threads);
        threads = NULL;
    }

    pthread_exit(NULL);

    return 0;
}

void *inc_counter_post(void *threadid) {
    int i = 0;

    for(i = 0; i < UPPER_BOUND; ++i) {
        usleep(0);
        counter++;
    }

    pthread_exit(NULL);
}

void *inc_counter_pre(void *threadid) {
    int i = 0;

    for(i = 0; i < UPPER_BOUND; ++i) {
        usleep(0);
        ++counter;
    }

    pthread_exit(NULL);
}
