/*
 * Samantha Foley
 *
 * CS 441/541: Hello World with Pthreads
 */
#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>

/* Number of threads to create */
#define NUM_THREADS 4

/*
 * Thread Start Function
 * Print Hello and exit
 *  *threadid : integer artificial thread ID
 */
void * thread_start( void *threadid );

int main(int argc, char **argv) {
    pthread_t threads[NUM_THREADS];
    int i, rc;

    /*
     * Create Threads
     */
    for( i = 0; i < NUM_THREADS; ++i ) {
        printf("main(): Creating thread id = %d\n", i);
	/*
	 * pthread_create takes the following argument types:
	 *    - pthread_t * : the thread to create
	 *    - pthread_attr_t *attr (or NULL)
	 *    - function pointer for the function to execute (see thread_start for more info)
	 *    - the argument to the function (cast to void *) which is the thread id
	 */
        rc = pthread_create(&(threads[i]), NULL, thread_start, (void*)(intptr_t)i);
        if( rc != 0 ) {
            printf("Error: Failed to create the thread\n");
            exit(-1);
        }
    }

    /*
     * Join Threads
     */
    for(i = 0; i < NUM_THREADS; ++i ) {
        rc = pthread_join( threads[i], NULL );
        if( rc != 0 ) {
            printf("Error: Failed to join thread\n");
            exit(-2);
        }
        printf("main(): join thread id = %d\n", i);
    }

    /*
     * Exit
     */

    return 0;
}

/*
 * The function that the thread executes must have the void * return type (but not return anything)
 * and take in one argument (as a void *) which is the thread id.
 * (See man page for pthread_create.)
 */
void * thread_start( void *threadid ) {
    int tid = (intptr_t)threadid;
    printf("I am a thread: Id = %d\n", tid);
    sleep(tid);
    pthread_exit( NULL );
}
