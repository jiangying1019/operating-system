#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>

#define LINELEN 128

int func(char **input, int opt);

int main(int argc, char **argv) {
    char *str = NULL;

    func(&str, 1);
    printf("String: %s\n", str);

    func(&str, 2);
    printf("String: %s\n", str);

    func(&str, 3);
    printf("String: %s\n", str);

    return 0;
}

int func(char **input, int opt) {
    char str[9] = "Hello!";
    char *dup = NULL;
    char *alt = NULL;

    alt = str;
    dup = strdup(str);
    str[2] = '\0';

    printf("String (len = %lu) \"%s\"\n", strlen(str), str);
    printf("String (len = %lu) \"%s\"\n", strlen(dup), dup);
    printf("String (len = %lu) \"%s\"\n", strlen(alt), alt);

    if( 1 == opt ) {
        (*input) = str; // bad
    }
    else if( 2 == opt ) {
        (*input) = dup; // bad
    }
    else if( 3 == opt ) {
        (*input) = strdup(dup); // correct
    }

    // Cleanup allocated memory
    free(dup);

    return 0;
}
