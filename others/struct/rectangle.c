#include <stdio.h>

struct point_t {
    double x;
    double y;
};
typedef struct point_t point_t;

struct rectangle_t {
    double width;
    double height;
    point_t upper_left;
    point_t lower_right;
};
typedef struct rectangle_t rectangle_t;

/************************************/
void display_rectangle(rectangle_t rect);
void display_point(point_t pt);

int create_point(double x, double y, point_t *pt);
int create_rectangle(double x, double y, 
                     double width, double height,
                     rectangle_t *rect);

/************************************/
int main(int argc, char **argv) {
    rectangle_t rect;

    create_rectangle(10, 7, 50, 40, &rect);

    display_rectangle(rect);

    return 0;
}
void display_rectangle(rectangle_t rect) {

    printf("Rect:\n");
    
    // print the upper_left
    display_point( rect.upper_left );
    // print the lower_right
    display_point( rect.lower_right );
    // print width/height
    printf("\t[w: %6.2f, h: %6.2f]\n", rect.width, rect.height);

}

void display_point(point_t pt) {
    printf("Point: (%6.2f, %6.2f)\n", pt.x, pt.y);
}

int create_point(double x, double y, point_t *pt) {
    // Both of the following mean the same thing
    // (*pt).x
    // pt->x

    (*pt).x = x;
    pt->y = y;

    return 0;
}

int create_rectangle(double x, double y, 
                     double width, double height,
                     rectangle_t *rect) {
    // Set width / height
    rect->width = width;
    (*rect).height = height;

    // Create a point - upper left
    (*rect).upper_left.x = x;
    (*rect).upper_left.y = y;

    // Create a point - lower right
    create_point( x + width, y + height, &( (*rect).lower_right ) );

    return 0;
}
