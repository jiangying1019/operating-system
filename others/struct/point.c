#include <stdio.h>

struct point_t {
    double x;
    double y;
};
typedef struct point_t point_t;

int create_point(double x, double y, point_t *pt);
void display_point( point_t pt );

int main(int argc, char **argv) {
    point_t pt;

    // Create a point
    create_point(10, -5, &pt);

    // Display the point
    display_point( pt );

    return 0;
}

int create_point(double x, double y, point_t *pt) {
    (*pt).x = x;
    pt->y = y;

    return 0;
}
void display_point( point_t pt ) {
    printf("Point: (%6.2f, %6.2f)\n", pt.x, pt.y);
}
