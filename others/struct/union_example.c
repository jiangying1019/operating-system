#include <stdio.h>

enum feels_like_type_t {
    FEEL_WIND_CHILL,
    FEEL_HEAT_INDEX
};
typedef enum feels_like_type_t feels_like_type_t;

union feels_like_t {
    double wind_chill;
    int heat_index;
};
typedef union feels_like_t feels_like_t;

struct weather_t {
    double temp;
    feels_like_t feel;
    feels_like_type_t type;
};
typedef struct weather_t weather_t;


void display_weather(weather_t w);

int main(int argc, char **argv) {
    weather_t summer;
    weather_t winter;

    printf("int    %lu\n", sizeof(int));
    printf("double %lu\n", sizeof(double));
    printf("Feel   %lu\n", sizeof(feels_like_t));

    summer.temp = 89;
    summer.feel.heat_index = 95;
    summer.type = FEEL_HEAT_INDEX;
    display_weather(summer);

    winter.temp = 20;
    winter.feel.wind_chill = 12;
    winter.type = FEEL_WIND_CHILL;
    display_weather(winter);

    return 0;
}

void display_weather(weather_t w) {
    if( w.type == FEEL_HEAT_INDEX ) {
        printf("%2.6f -- %10d\n",
               w.temp,
               w.feel.heat_index);
    }
    else {
        printf("%2.6f -- %6.2f\n",
               w.temp,
               w.feel.wind_chill);
    }

}
