#include <stdio.h>
#include <string.h>
#include <stdlib.h>

enum op_type_t {
    OP_ADD,
    OP_SUB,
    OP_MULT,
    OP_DIV,
    OP_MOD,
    OP_ERR = -1
};
typedef enum op_type_t op_type_t;

//const int OP_ERR = 10;

void display_op(op_type_t op);

int get_op_str(op_type_t op, char **str);

int main(int argc, char *argv[] ) {
    int alt;
    op_type_t tmp;
    char *value = NULL;

    tmp = 42;
    alt = OP_DIV;
    printf("Alt  = %2d\n", alt);
    printf("Tmp  = %2d\n", tmp);
    printf("----------------------\n");
    printf("Add  = %2d\n", OP_ADD);
    printf("Sub  = %2d\n", OP_SUB);
    printf("Mult = %2d\n", OP_MULT);
    printf("Div  = %2d\n", OP_DIV);
    printf("Mod  = %2d\n", OP_MOD);
    printf("Err  = %2d\n", OP_ERR);
    printf("----------------------\n");
    display_op(tmp);
    display_op(alt);

    get_op_str(OP_ADD, &value);
    printf("Value = %s\n", value);
    // The strdup() inside get_op_str allocated memory on the heap.
    // So make sure to free that memory when we are finished with it.
    free(value);
    value = NULL;

    return 0;
}

void display_op(op_type_t op) {
    switch(op) {
    case OP_ADD:
        printf("Add\n");
        break;
    case OP_SUB:
        printf("Sub\n");
        break;
    case OP_MULT:
        printf("Mult\n");
        break;
    case OP_DIV:
        printf("Div\n");
        break;
    case OP_MOD:
        printf("Mod\n");
        break;
    case OP_ERR:
        printf("Error\n");
        break;
    default:
        printf("Unknown %d\n", op);
        break;
    }
}

int get_op_str(op_type_t op, char **str) {
    switch(op) {
    case OP_ADD:
        (*str) = strdup("Add");
        break;
    case OP_SUB:
        (*str) = strdup("Sub");
        break;
    case OP_MULT:
        (*str) = strdup("Mult");
        break;
    case OP_DIV:
        (*str) = strdup("Div");
        break;
    case OP_MOD:
        (*str) = strdup("Mod");
        break;
    case OP_ERR:
        (*str) = strdup("Error");
        break;
    default:
        return -1;
        break;
    }

    return 0;
}
