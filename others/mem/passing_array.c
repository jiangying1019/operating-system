#include <stdio.h>
#include <stdlib.h>

void display(int ary[], int len);
int get_array(int len, int ** array);

int main(int argc, char **argv) {
    int *ary1 = NULL;
    int ary3[] = {10, 20, 30};
    int rtn;

    //display(ary3, 3);
    printf("----------------------------\n");
    rtn = get_array(5, &ary1);
    display(ary1, 5);
    printf("----------------------------\n");

    free(ary1);
    ary1 = NULL;

    return 0;
}

int get_array(int len, int ** array) {
    int i;

    // Allocate some space on the heap
    (*array) = (int*) malloc( sizeof(int) * len );
    if( NULL == (*array) ) {
        fprintf(stderr, "Error: failed to allocate memory\n");
        return -1;
    }


    for(i = 0; i < len; ++i) {
        (*array)[i] = 100 + i + (len * 10);
    }

    return 0; 
}
 
void display(int ary[], int len) {
    int i;
    if( ary == NULL ) {
        printf("Error: Array is NULL\n");
        return;
    }
    for(i = 0; i < len; ++i ) {
        printf("%2d = %d\n", i, ary[i]);
        ary[i] += 2;
    }
}
