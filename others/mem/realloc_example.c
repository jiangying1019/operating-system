#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int get_next_double(double *value);

int main(int argc, char **argv) {
    double *ary = NULL;
    double value;
    int ret, i;
    int len = 0;

    while( 1 ) {
        printf("Bytes: %5lu (%5d elements) (%15p)\n",
               (sizeof(ary) * len), len, ary);

        printf("Input a number: ");

        ret = get_next_double(&value);
        if( ret != 0 ) {
            break;
        }

        printf("Value = %.2f\n", value);

        // Allocate some space
        ary = (double*) realloc( ary, sizeof(double) * (len + 1) );
        if( NULL == ary ) {
            fprintf(stderr, "Error: Failed to allocate memory\n");
            exit(-1);
        }

        // Save the value
        ary[len] = value;
        ++len;


        printf("---------------\n");
        printf("----------- %3d\n", len);
        for(i = 0; i < len; ++i ) {
            printf("%2d) %.2f\n", i, ary[i]);
        }
    }


    return 0;
}









int get_next_double(double *value) {
    char *buffer = NULL;
    char *fgets_rtn = NULL;

    // Dynamically create the buffer
    buffer = (char*)malloc(sizeof(char) * 256);
    if( NULL == buffer ) {
        fprintf(stderr, "Error: Failed to allocate buffer\n");
        return -2;
    }

    // TODO (show getline)
    
    // Get a line from the user
    fgets_rtn = fgets(buffer, 256, stdin);
    if( NULL == fgets_rtn ) {
        return -1;
    }

    // Convert to a double
    (*value) = strtod(buffer, NULL);

    // Free the buffer now that we are done with it
    if( NULL != buffer ) {
        free(buffer);
        buffer = NULL;
    }

    return 0;
}
