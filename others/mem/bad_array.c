#include <stdio.h>

void display(int ary[], int len);
int * get_array(int len);

int main(int argc, char **argv) {
    int *ary1 = NULL;
    int ary3[] = {10, 20, 30};

    //display(ary3, 3);
    printf("----------------------------\n");
    ary1 = get_array(5);
    display(ary1, 5);
    printf("----------------------------\n");

    return 0;
}

int * get_array(int len) {
    int i;
    int local[len];

    for(i = 0; i < len; ++i) {
        local[i] = 100 + i + (len * 10);
    }

    return local; // Bad bad bad...
}
 
void display(int ary[], int len) {
    int i;
    if( ary == NULL ) {
        printf("Error: Array is NULL\n");
        return;
    }
    for(i = 0; i < len; ++i ) {
        printf("%2d = %d\n", i, ary[i]);
        ary[i] += 2;
    }
}
