#include <stdio.h>
#include <stdlib.h>

void display(int ary[], int len);
int * get_array(int len);

int main(int argc, char **argv) {
    int *ary1 = NULL;
    int ary3[] = {10, 20, 30};

    //display(ary3, 3);
    printf("----------------------------\n");
    ary1 = get_array(5);
    display(ary1, 5);
    printf("----------------------------\n");

    free(ary1);
    ary1 = NULL;

    return 0;
}

int * get_array(int len) {
    int i;
    int *local = NULL;

    // Allocate some space on the heap
    local = (int*) malloc( sizeof(int) * len );
    if( NULL == local ) {
        fprintf(stderr, "Error: failed to allocate memory\n");
        return NULL;
    }


    for(i = 0; i < len; ++i) {
        local[i] = 100 + i + (len * 10);
    }

    return local; // Better now - heap pointer
}
 
void display(int ary[], int len) {
    int i;
    if( ary == NULL ) {
        printf("Error: Array is NULL\n");
        return;
    }
    for(i = 0; i < len; ++i ) {
        printf("%2d = %d\n", i, ary[i]);
        ary[i] += 2;
    }
}
