#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
    FILE *fd = NULL;
    char line[256];
    char *fgets_rtn = NULL;
    char *filename = "txt/king.txt";
    FILE *wfd = NULL;

    // Check if the reading file exists
    // TODO

    // Open the file for reading
    fd = fopen(filename, "r");
    if( NULL == fd ) {
        fprintf(stderr, "Error: Failed to open the file\n");
        return -1;
    }

    // Open another file for writing
    wfd = fopen("txt/copy.txt", "w");
    if( NULL == wfd ) {
        fprintf(stderr, "Error: Failed to open the file\n");
        exit( -1 );
    }

    // Keep reading until the end-of-file
    while( 0 == feof(fd)) {
        // Read characters into our buffer
        fgets_rtn = fgets(line, 256, fd);
        if( NULL == fgets_rtn ) {
            break;
        }

        // Strip off the newline
        if( line[strlen(line)-1] == '\n' ) {
            line[strlen(line)-1] = '\0';
        }

        // Echo the line to the console
        fprintf(stdout, "Line: \"%s\"\n", line);

        // TODO Print the line to the (copy) file
        fprintf( wfd, "Line: \"%s\"\n", line);
    }

    // Close the file
    fclose(fd);
    fclose(wfd);

    return 0;
}
