#include <stdio.h>
#include <string.h>

#define LINELEN 256

int main(int argc, char *argv[]) {
    FILE *fd = NULL;
    char *fgets_rtn = NULL;
    char line[ LINELEN ];

    // Open the file
    fd = fopen("txt/king.txt", "r");

    // Keep reading until the end-of-file marker
    while( 0 == feof( fd ) ) {
       // Read characters into the buffer
        fgets_rtn = fgets( line, LINELEN, fd);
        if( NULL == fgets_rtn ) {
            break;
        }

       // Strip off the newline
        if( '\n' == line[ strlen(line) -1 ] ) {
            line[ strlen(line) -1 ] = '\0';
        }
       // Echo the line
        printf("Echo: \"%s\"\n", line);
    }

    // Close the file
    fclose(fd);

    return 0;
}
