#include <stdio.h>
#include <string.h>

#define LINELEN 64

int main(int argc, char **argv) {
    char *fgets_rtn = NULL;
    char buffer[ LINELEN ];
    int x = 10;

    do {
        printf("X = %d\n", x);
        fgets_rtn = fgets( buffer, LINELEN, stdin );
        if( fgets_rtn != NULL ) {
            if( '\n' == buffer[ strlen(buffer)-1 ] ) {
                printf("Remove newline\n");
                buffer[ strlen(buffer)-1 ] = '\0';
            }
            printf("Echo: \"\%s\" (%d)\n", buffer, x);
        }
    } while( fgets_rtn != NULL );

    return 0;
}
