/*
 * TODO: Fillin
 */
#include <stdio.h>
#include <string.h>

int main(int argc, char **argv) {
    int i;
    int len;

    for( i = 0; i < argc; ++i ) {
        printf("%d) \"%s\"\n", i, argv[i]);
        // Be careful with null terminated strings! 
        if( 2 == i ) {
            len = (int)strlen( argv[i] );
            argv[i][len] = '!';
            argv[i][len+1] = 'z';
            printf("!!!! %d) \"%s\"\n", i, argv[i]);
         }
    }

    return 0;
}
