/*
 * S. Foley
 *
 * A simple fork program
 * make some edits
 */
#include <stdio.h>
#include <stdlib.h>  
#include <sys/types.h> /* fork, exec, waitpid */
#include <unistd.h>    /* fork, exec, waitpid */
#include <sys/wait.h>  /* fork, exec, waitpid */

int main(int argc, char **argv) {
    pid_t c_pid = 0;
    int rtn, status;

    printf("Before fork()\n");
    // Fork a process
    c_pid = fork();
    // Check, if there was an error
    if( c_pid < 0 ) {
        printf("Error: fork() failed!!!\n");
        exit(-1);
    }
    // Check, if I am the child process
    else if( 0 == c_pid ) {
        printf("Child: My PID is %d, my parent is %d\n", getpid(), getppid() );
        sleep(10);
        exit( 42 );
    }
    // Otherwise I am the parent process
    else {
        printf("Parent: My PID is %d, my child is %d\n", getpid(), c_pid );
        rtn = waitpid( c_pid, &status, 0);
        printf("Child finsihed. Cleanup... (Status: %d) (rtn = %d)\n", WEXITSTATUS(status), rtn);
    }
    printf("All done\n");

    return 0;
}
