#include <stdio.h>


/*
 * Run a comparison test of v1 and v2
 *
 * Parameters:
 *   lhs : numerator
 *   rhs : denominator
 *
 * Returns:
 *   0 on succcess, negative value otherwise
 */
int run_test(int lhs, int rhs);

/*
 * Integer division with divide by 0 protection
 * Version 1
 *
 * Parameters:
 *   num : numerator
 *   dem : denominator
 *
 * Returns:
 *   -1 if demoninator is 0
 *   integer division otherwise
 */
int int_division_v1(int num, int dem);

/*
 * Integer division with divide by 0 protection
 * Version 2
 *
 * Parameters:
 *   num : numerator
 *   dem : denominator
 *   result : Result of the calculation (pass-by-reference)
 *
 * Returns:
 *   0 on success
 *   -1 if demoninator is 0
 */
int int_division_v2(int num, int dem, int *result);
