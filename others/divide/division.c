/*
 */
#include "division.h"

int main(int argc, char **argv) {

    run_test( 10, 3);
    printf("------------------\n");
    run_test( -5, 0);
    printf("------------------\n");
    run_test( -5, 3);

    return 0;
}

int run_test(int lhs, int rhs) {
    int ret, result;

    result = int_division_v1(lhs, rhs);
    if( result == -1 ) {
        fprintf(stderr, "Error: Divide by 0\n");
    } else {
        printf("v1) %3d / %3d = %3d\n",
               lhs, rhs, result);
    }

    result = -9999;
    ret = int_division_v2(lhs, rhs, &result);
    if( ret != 0 ) {
        fprintf(stderr, "Error: Divide by 0\n");
    } else {
        printf("v2) %3d / %3d = %3d\n",
               lhs, rhs, result);
    }

    return 0;
}

int int_division_v1(int num, int dem) {
    if( 0 == dem ) {
        return -1;
    }

    return num / dem;
}

int int_division_v2(int num, int dem, int *result) {
    if( 0 == dem ) {
        return -1;
    }

    //result = num / dem; --> Don't change the memory address!!!
    (*result) = num / dem;

    return 0;
}
