#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define LINELEN  1024

//To count the number of '$' characters
int count_dollor(char *line)
{
  int i=0;
  int num_dollor = 0;
 
  for(i=0;i<strlen(line);i++)
    { 
      if(line[i]-'$'==0)
       	num_dollor=num_dollor+1;	
    }
  //DES encode 
  if(num_dollor==0)
    return 0;
  //line has three '$'
  else if(num_dollor==3)
    return 3;
  else
    return -1;
}

//return all kinds of encode method
char * encode_method(int hash_id)
{
  char *encode_way;
  if(hash_id==0)
    encode_way = "DES";
 else if(hash_id==1)
    encode_way = "MD5";
 else if(hash_id==5)
   encode_way = "SHA-256";
 else if(hash_id==6)
   encode_way = "SHA-512";
 else
   encode_way = NULL;
  return encode_way;
}

 /* 
 * Process a single line of the file 
 * 
 * Parameters: 
 * line: Line to process from the file 
 * username: Username from the line (pass-by-reference)
 * hash_id: Hash method id (pass-by-reference) [0=DES, 1=MD5, 5=SHA-256, 6=SHA-512]
 * hash_salt: Salt used for this hash without id (pass-by-reference)
 * hash_pwd: The hashed password without salt or id (pass-by-reference)
 * 
 * Returns:
 * -1 if line is not a valid username/password line
 * 0 on success
 */ 

int process_line(char *line,char **username,int *hash_id, char **hash_salt,char **hash_pwd)
{ 
  char *temp;
  char *mycopy;
  char buffer[20] = {'\0'};
  mycopy = strdup(line);
  temp = strdup(line);
  
  int  wordlen = 0;
  
  if(strchr(line, ':') == NULL){
    printf("Error : Input line missing the ':' seperator. Could not find username/password! \n\n");
    return -1;
  }

  *username = strtok(mycopy, ":");
  //mycopy now is username
 
  if(strlen(*username)<3)
    {
      printf("Error: Username is less than three characters.\n\n");
      return -1;
    }

  //current position is at first $
  temp = strdup(&line[strlen(*username)+1]);
  //start from the hash_id or passwd(DES encoding)
 
    if(count_dollor(line)==-1)
    {
      printf("Error: The number of '$' are not 1 or 3!\n\n");
      return -1;
    }
		
    if(count_dollor(line)==0)
      { *hash_salt = buffer;
	//strncpy needs space and initialize
	 strncpy(*hash_salt, temp,2);
	 *hash_pwd = strdup(&temp[2]);
        *hash_id = 0;
      if(strlen(*hash_pwd)!=11)
	{ wordlen = strlen(*hash_pwd);
	  printf("Error : DES password length incorrect!Error: DES Password length incorrect. Expected 11, Actual %d \n\n", wordlen);
	  return -1;
	}
    }
 
    //If the number of '$' are 3
    if(count_dollor(line)==3)
    {
       mycopy =strdup(&temp[1]);
       *hash_id = atoi(strtok(mycopy, "$"));
       //mycopy is hash_id now
  
       //MD5 encode method
      if(*hash_id==1)
	{
	  //get hash_salt
	  *hash_salt = strtok(NULL, "$"); 
	   if(strlen(*hash_salt)!=8)
	     { wordlen = strlen(*hash_salt);
	       printf("Error: MD5 Salt length incorrect. Expected 8, Actual %d \n\n", wordlen);
	       return -1;
	     }
	   //get hash_pwd
	   *hash_pwd = strtok(NULL, "$");
	 
	    if(strlen(*hash_pwd) != 22)
	      { wordlen = strlen(*hash_pwd);
	       printf("Error : MD5 password length incorrect. Expected 22, Actual %d \n\n",wordlen );
	       return -1;
	     }
	}
      else if(*hash_id==5)
	{  
	  *hash_salt = strtok(NULL, "$");
	   if(strlen(*hash_salt)!=16)
	     { wordlen = strlen(*hash_salt);
	       printf("Error: SHA-256 Salt length incorrect. Expected 16, Actual %d \n\n", wordlen);
	       return -1;
	     }
	   //  *hash_salt[16] = '\0';
	   *hash_pwd = strtok(NULL, "$");
	    if(strlen(*hash_pwd) != 43)
	      { wordlen = strlen(*hash_pwd);
	       printf("Error : SHA-256 password length incorrect. Expected 43, Actual %d \n\n", wordlen);
	       return -1;
	     }
	  
	}
      else if(*hash_id==6)
	{  
	  *hash_salt = strtok(NULL, "$");
	   if(strlen(*hash_salt)!=16)
	     { wordlen = strlen(*hash_salt);
	       printf("Error: SHA-512 Salt length incorrect. Expected 16, Actual %d \n\n ", wordlen);
	       return -1;
	     }
	   // *hash_salt[16] = '\0';
	   *hash_pwd = strtok(NULL, "$");
	    if(strlen(*hash_pwd) != 86)
	      { wordlen = strlen(*hash_pwd);
	       printf("Error : SHA-512 password length incorrect. Expected 86, Actual %d \n\n", wordlen);
	       return -1;
	     }
	}
      else
	{  printf("Error: Unknow hash_id!\n\n");
	  return -1;
	}
    }
     return 1;

}


int main(int argc, char *argv[]) {
    FILE *fd = NULL;
    char *fgets_rtn = NULL;
    char line[ LINELEN ];

    char *username = NULL;
    int  hash_id = 0; //convert to int
    char *hash_salt = NULL;
    char *hash_pwd = NULL;

    int num_valid=0, num_invalid=0, num_DES=0, num_MD5=0, num_SHA_256=0, num_SHA_512=0;
    
    // Open the file

    fd = fopen("test.txt", "r");

    // Keep reading until the end-of-file marker                                                                                                                                                            
    while( 0 == feof( fd ) ) {
       // Read characters into the buffer                                                                                                                                                                   
        fgets_rtn = fgets( line, LINELEN, fd);
        if( NULL == fgets_rtn ) {
            break;
        }

       // Strip off the newline                                                                                                                                                                             
        if( '\n' == line[ strlen(line) -1 ] ) {
            line[ strlen(line) -1 ] = '\0';
        }
       // Echo the line

	printf("Processing: %s\n", line);

	//process each line to see whether the line is valid or not
 
       	if(process_line(line, &username, &hash_id, &hash_salt, &hash_pwd)==1)
	  {
	    //if the line is valid, print username, encode way, hash salt, hash password
	    printf(" %s / %s / %s / %s\n\n", username, encode_method(hash_id), hash_salt, hash_pwd);
	    if(hash_id==0)
	      num_DES++;
	    if(hash_id==1)
	      num_MD5++;
	    if(hash_id==5)
	      num_SHA_256++;
	    if(hash_id==6)
	      num_SHA_512++;
	    num_valid++;
	   }
	else
	  num_invalid++;
	
	  }

    // Close the file                                                                                                                                                                                       
    fclose(fd);
    //print the summary
    printf("Number of valid   : %d\n", num_valid);
    printf("Number of invalid : %d\n", num_invalid);
    printf("Number of DES     : %d\n", num_DES);
    printf("Number of MD5     : %d\n", num_MD5);
    printf("Number of SHA-256 : %d\n", num_SHA_256);
    printf("Number of SHA-512 : %d\n", num_SHA_512);
    return 0;
}
