CS441/541 C Lab2  File Processing

Author(s): Jiang Ying

Date: 9/20/2016

How to build the software
ying@ubuntu:~/clab_2$ make
make: Nothing to be done for 'all'.


How to use the software
ying@ubuntu:~/clab_2$ ./fsplit 


How the software was tested
Test cases:

ying@ubuntu:~/clab_2$ ./fsplit 
Processing: alice:GMuMeMoqUogqE
 alice / DES / GM / uMeMoqUogqE

Processing: bob:$1$kqavfSjz$vro/DEMNfXLOjzvzocJVe.
 bob / MD5 / kqavfSjz / vro/DEMNfXLOjzvzocJVe.

Processing: carol$Rp56HOCefht66
Error : Input line missing the ':' seperator. Could not find username/password! 

Processing: dave:$1$kqavfSjz$vro/DEMNfXLOjocJVe.
Error : MD5 password length incorrect. Expected 22, Actual 19 

Processing: edgar:$1$mlfaifixe$qkIXmDedUyXqGulKunRDI0
Error: MD5 Salt length incorrect. Expected 8, Actual 9 

Processing: frida:$110$VD9MCr8AX1U6OgWi$eV5MaojiM/.H79EYkRj/RgaBmg7XNAEmFVbvqBflKrD
Error: Unknow hash_id!

Processing: grady:$5$baRUfhiHm/2PEzzD$qcdhkY5VNnjgkRsVx6mojO6G/VIaJHCf6.G1cxNZlc9
 grady / SHA-256 / baRUfhiHm/2PEzzD / qcdhkY5VNnjgkRsVx6mojO6G/VIaJHCf6.G1cxNZlc9

Number of valid   : 3
Number of invalid : 4
Number of DES     : 1
Number of MD5     : 1
Number of SHA-256 : 1
Number of SHA-512 : 0


Known bugs and problem areas



