/*
 *Ying Jiang
 *
 * CS 441/541: Disk Algorithm Simulator (Project 6)
 */
#include "scheduler.h"


int number_of_cylinders = 0;
int number_of_reqs = 0;
int head = 0;
int direction = -1;
char *file_name = NULL;


int main(int argc, char **argv) {
  parse_argus(argc, argv);
  //get request array and the number of cylinders
  getnum_reqs();
  //check whether the arguments is valid
  check_args();
  int req_array[number_of_reqs];
  //read each line to the req_array
  read_file(req_array);
  print(req_array);
  
    return 0;
}

void parse_argus(int args, char **argv){
  int i=1, hflag=0, dflag=0, fflag=0;
  if(args < 6){
    printf("Missing parameters!\n");
    exit(-1);
  }else{
    while(i<args){
      if(strcmp(argv[i],"-h") == 0 && (i+1<args)){
	if(strcmp(argv[i+1],"0")==0){
	  head = 0;
	  hflag = 1;
	  i = i+2;
	}
	else if((head = atoi(argv[i+1]))!=0){
	  i = i+2;
	  hflag = 1;
	}else{
	  printf("-h requires a posotive number as argument\n");
	  exit(-1);
	}
      }
      else if(strcmp(argv[i], "-d")==0&&(i+1<args)){
	if(strcmp(argv[i+1],"0")==0){
	  i=i+2;
	  dflag = 1;
	  direction = 0;
	}
	else if((direction=atoi(argv[i+1]))!=0){
	  i=i+2;
	  dflag = 1;
	}else{
	  printf("-d requires a posotive number as argument\n");
	  exit(-1);
	}
      }else if(file_name==NULL){
	file_name = argv[i];
	i = i+1;
	fflag = 1;
      }else i = i+1;
    }//end while

    if(hflag == 0){
      printf("Error: Must supply the -h option \n");
      exit(-1);
    }
    if(dflag == 0){
      printf("Error: Must supply the -d option \n");
      exit(-1);
    }
    if(fflag==0){
      printf("Error: Did not supply a scheduler file\n");
      exit(-1);
    }
  }//end else
}

//check whether the -h , -d arguments have the valid value 
void check_args(){
  if(head > number_of_cylinders){
    printf("Error: Must supply an integer argument greater than 0 and\nless than the number of cylinders on the disk for the -h option\nThis disk has %d cylinders\n",number_of_cylinders);
    exit(-1);
  }

  if(direction != 0 && direction != 1){
    printf("Error: direction is 0 or 1\n");
    exit(-1);
  }
}
//get requests and cylinders
void getnum_reqs(){
 FILE *fd = NULL;
 char *fgets_rtn = NULL;
 char line[ MAX_INPUT_LINE ];
  fd = fopen(file_name, "r");
  //get number of cylinders
  fgets_rtn = fgets( line, MAX_INPUT_LINE, fd);
        if( NULL == fgets_rtn ) {
	  exit(-1);
        }   
  
        if( '\n' == line[ strlen(line) -1 ] ) {
            line[ strlen(line) -1 ] = '\0';
        }
	
	number_of_cylinders = atoi(line);
	//get number of requests
	fgets_rtn = fgets( line, MAX_INPUT_LINE, fd);
        if( NULL == fgets_rtn ) {
	  exit(-1);
        }   
	
        if( '\n' == line[ strlen(line) -1 ] ) {
            line[ strlen(line) -1 ] = '\0';
        }
	
	number_of_reqs = atoi(line);
	fclose(fd);
	fflush(NULL);
}
//read the request queue and put the number in an array 
void read_file(int req_array[]){
  FILE *fd = NULL;
  char *fgets_rtn = NULL;
  char line[ MAX_INPUT_LINE ];
  int  i = 0;
   fd = fopen(file_name, "r");
 
     while(0 == feof( fd )){
	 fgets_rtn = fgets( line, MAX_INPUT_LINE, fd);
        if( NULL == fgets_rtn ) {
            break;
        }

        if( '\n' == line[ strlen(line) -1 ] ) {
            line[ strlen(line) -1 ] = '\0';
        }

	if(i>=2){
	req_array[i-2] = atoi(line);
	}
    i++;
   }//end of while
       fclose(fd);
}

//print information
void print(int req_array[]){
 
  printf("#--------------------------------------------\n");
  printf("# Queue File               : %s\n", file_name);
  printf("# Num. Disk Cylinders      : %d\n", number_of_cylinders);
  printf("# Head Position            : %d\n", head);
  if(direction == 0)
    printf("# Head Direction           : Toward 0\n");
  else
    printf("# Head Direction           : Toward last cylinder\n");
  
  printf("#--------------------------------------------\n");
  printf("#    %d      FCFS\n", fcfs(req_array));
  printf("#    %d      SSTF\n", sstf(req_array));
  printf("#    %d      SCAN\n", scan(req_array));
  printf("#    %d      C-SCAN\n", cscan(req_array));
  printf("#    %d      LOOK\n", look(req_array));
  printf("#    %d      C-LOOK\n", clook(req_array));
  
}

//first come first serve 
int fcfs(int req_array[]){
  int i = 0;
  int sum = abs(req_array[0]-head);
  
  for(i=1;i<number_of_reqs;i++){
    sum = sum + abs(req_array[i]-req_array[i-1]);
  }
  return sum;
}

/*
 *sstf
 */
//get the next position with the minimum distance from current position
int min_distance(int curr_value, int req_array[], int flag_array[]){
  int min = 10000000;
  int i = 0;
  int distance = 0;
  int position = -1;
  
  for(i=0;i<number_of_reqs;i++){
    if(flag_array[i]==0){
      distance = abs(req_array[i]-curr_value);
      if(distance < min){
	min = distance;
	position = i;
      }
    }
  }
  return position;
}

//sortest seek time first
//get the current , get the next position(has the minimum distance)
int sstf(int req_array[]){
  int flag_array[number_of_reqs];
  int i=0, sum = 0, curr_value = 0, curr_posi = -1, min_posi = -1, min_value = 0;
  for(i=0;i<number_of_reqs;i++){
    flag_array[i] = 0;
  }
  curr_posi = min_distance(head, req_array, flag_array);
  sum = abs(head - req_array[curr_posi]);

  for(i=0;i<number_of_reqs-1;i++){
    curr_value = req_array[curr_posi];
    flag_array[curr_posi] = 1;
    min_posi = min_distance(curr_value, req_array, flag_array);
    min_value = req_array[min_posi];
    sum = sum + abs(curr_value - min_value);
    curr_posi = min_posi;
  }
  return sum;
}

/*
 *scan
 */
//find the max value in the request array
int find_max(int array[]){
  int i=0;
  int max = array[0];
  for(i=0;i<number_of_reqs;i++){
    if(array[i]>max){
      max = array[i];
    }
  }
  return max;
}
//find the minimum value in the request array
int find_min(int array[]){
  int i=0;
  int min = array[0];
  for(i=0;i<number_of_reqs;i++){
    if(array[i]<min)
      min = array[i];
  }
  return min;
}
//scan
//Move disk arm	from one end to	the other servicing requests on	the way. Reverse direction when	you hit	the other end
int scan(int req_array[]){
  int sum = 0;
  int max = find_max(req_array);
  int min = find_min(req_array);
  if(head == 0){
    return max;
  }
  if(head >= max && direction == 0){
    return head - min;
  }

  if(head >= max && direction == 1){
    return number_of_cylinders - 1 - head + number_of_cylinders -1 - min;
  }

  if(head <= min && direction == 0){
    return head + max;
  }
  if(head <= min && direction == 1){
    return max - head;
  }

  if(direction == 0){
    sum = head + max;
  }
  if(direction == 1){
    sum = number_of_cylinders -head + number_of_cylinders - min -2;
  }
  
  return sum;
}

/*
 *cscan
 */

//find the two numbers around head
//if direction is 0 return value next to head which is bigger than head
//id direction is 1 return value before head which is less than head 
//it is for cscan and clook
int int_next_to_head(int array[]){
  int i = 0;
  int min_num_next_head = 100000000, max_num_next_head = 100000000, min_value = 0, max_value = 0;
  for(i=0;i<number_of_reqs;i++){
    if(array[i] < head){
      if(head-array[i]<min_num_next_head){
	min_num_next_head = head-array[i];
	min_value =array[i];
      }
    }

    if(array[i]>head){
      if(array[i]-head < max_num_next_head){
	max_num_next_head = array[i] - head;
	max_value =array[i];
      }
    }
  }

     if(direction == 0){
      return max_value;
    }else{
      return min_value;
    }
   
}
//circular scan
int cscan(int req_array[]){
  int sum = 0;
  int min = find_min(req_array);
  int max = find_max(req_array);

  if(head == min && direction == 1){
    return max;
  }
  
  if(head < min && direction == 1){
    return max - head;
  }
  
  if(head == min && direction ==0){
     return number_of_cylinders -1 - min + number_of_cylinders -1 + head -1;
  }
  
  if(head < min && direction == 0){
    return number_of_cylinders -1 - min + number_of_cylinders -1 + head;
  }

  
  if(head >= max && direction == 0){
    return number_of_cylinders -1 - min;
  }

  if(head == max && direction == 1){
     return number_of_cylinders -1 + max-2;
  }
  
  if(head > max && direction == 1){
     return number_of_cylinders -1 + max;
  }
 
  if(direction == 0){
    sum = head + number_of_cylinders - int_next_to_head(req_array)+number_of_cylinders-2;
  }
  if(direction == 1){
     sum = number_of_cylinders-1 -head + int_next_to_head(req_array)+number_of_cylinders - 1;
  }
  
  return sum;
}

/*
 *look
 */

int look(int req_array[]){
  int min = find_min(req_array);
  int max = find_max(req_array);
  if(head <= min){
      return max - head;
  }else if(head >= max){
    return head - min;
  } else if(direction == 0){
    return head - min + max - min;
  }else if(direction == 1){
    return max - head + max - min;
  }else
  return 0;
}

/*
 *clook
 */
int clook(int req_array[]){
  int min = find_min(req_array);
  int max = find_max(req_array);

  if(head == min && direction == 0){
    return max - head + max - min -1;
  }
  
  if(head < min && direction == 0){
    return max - head + max - min;
  }
  
  if(head <= min && direction == 1){
    return max - head;
  }

  
  if(head >= max && direction == 0){
    return head - min;
  }

  if(head == max && direction == 1){
    return head - min + max - min -2;
  }
  
  if(head > max && direction == 1){
    return head - min + max - min;
  }
 

  if(direction == 0){
    return head - min + max - min + max - int_next_to_head(req_array);
  }
  if(direction == 1){
    return max - head + max - min + int_next_to_head(req_array)-min;
  }
  return 0;
}
