/*
 *
 *
 * CS 441/541: Disk Algorithm Simulator (Project 6)
 */
#include <stdio.h>
/* For atoi */
#include <stdlib.h>
/* For String operations - strlen */
#include <string.h>
/* For isdigit */
#include <ctype.h>
/* For bool type */
#include <stdbool.h>


/******************************
 * Defines
 ******************************/
// Maximum per-line length from the files
#define MAX_INPUT_LINE 1024


/******************************
 * Structures
 ******************************/


/******************************
 * Global Variables
 ******************************/


/******************************
 * Function declarations
 ******************************/

/*parse arguments
 */
void parse_argus(int argc, char **argv);

/*
 *Read a file and put the value of each line to an array
 *return the array
 */
void read_file(int req_array[]);

void getnum_reqs();
/*
 *Print information
 */
void print(int req_array[]);

/*
 *fsfc
 */
int fcfs(int req_array[]);

/*
 *sstf
 */

int min_distance(int curr_posi, int req_array[], int flag_arrya[]);
int sstf(int req_array[]);

/*
 *scan
 */

int find_max(int array[]);
int find_min(int array[]);

int scan(int req_array[]);

/*
 *cscan
 */
//find two values around head
int int_next_to_head(int array[]);
int cscan(int req_array[]);

/*
 *look
 */
int look(int req_array[]);
/*
 *clook
 */
int clook(int req_array[]);

/*
 *check if the argument is valid
 */
void check_args();

