# CS441/541 Project 5

## Author(s):

Ying Jiang


## Date:

12/02/2016


## Description:

This project implements a disk scheduling simulator. 


## How to build the software

ying@ubuntu:~/project5/cs441-f16-project-5$ make
gcc -o scheduler -Wall -g -O0 scheduler.c
ying@ubuntu:~/project5/cs441-f16-project-5$ 



## How to use the software

• Required: -h #
The current head position. The head position can range from 0 to one less than the number of cylinders in the disk deﬁned in the ﬁle.
If this parameter is not provided then your program will print an error and exit.
If this parameter is out-of-range then your program will print an error and exit.
Note that you will need to read the ﬁle before checking the bounds of this command line option to properly check that the value is in bounds.
• Required: -d #
The current direction of travel of the disk head.
If the number is 0 then the disk head is moving toward cylinder 0.
If the number is 1 then the disk head is moving toward the last cylinder on the disk.
If the number is neither 0 not 1 then your program will print an error and exit.

For example:
ying@ubuntu:~/project5/cs441-f16-project-5$ ./scheduler -h 5 -d 1 given-tests/level1.txt


## How the software was tested

make check > output.txt
It passed all test cases.

## Test Suite

1. Test invalid arguments
ying@ubuntu:~/project5/cs441-f16-project-5$ ./scheduler given-tests/level2.txt -h 1000 -d 1
Error: Must supply an integer argument greater than 0 and
less than the number of cylinders on the disk for the -h option
This disk has 50 cylinders

ying@ubuntu:~/project5/cs441-f16-project-5$ ./scheduler -h 0 given-tests/level2.txt -d 
Missing parameters!

ying@ubuntu:~/project5/cs441-f16-project-5$ ./scheduler -h 0 -d 2 given-tests/level2.txt 
Error: direction is 0 or 1

ying@ubuntu:~/project5/cs441-f16-project-5$ ./scheduler -d 1 given-tests/level2.txt -h 300 
Error: Must supply an integer argument greater than 0 and
less than the number of cylinders on the disk for the -h option
This disk has 50 cylinders

ying@ubuntu:~/project5/cs441-f16-project-5$ ./scheduler -h 0 given-tests/level2.txt 
Missing parameters!

ying@ubuntu:~/project5/cs441-f16-project-5$  ./scheduler given-tests/level2.txt 
Missing parameters!

2. Test boundry values of /test/test1.txt
ying@ubuntu:~/project5/cs441-f16-project-5$ ./scheduler -h 0 tests/test1.txt -d 0
#--------------------------------------------
# Queue File               : tests/test1.txt
# Num. Disk Cylinders      : 100
# Head Position            : 0
# Head Direction           : Toward 0
#--------------------------------------------
#    308      FCFS
#    86      SSTF
#    86      SCAN
#    197      C-SCAN
#    86      LOOK
#    171      C-LOOK
ying@ubuntu:~/project5/cs441-f16-project-5$ ./scheduler -h 0 tests/test1.txt -d 1
#--------------------------------------------
# Queue File               : tests/test1.txt
# Num. Disk Cylinders      : 100
# Head Position            : 0
# Head Direction           : Toward last cylinder
#--------------------------------------------
#    308      FCFS
#    86      SSTF
#    86      SCAN
#    86      C-SCAN
#    86      LOOK
#    86      C-LOOK
ying@ubuntu:~/project5/cs441-f16-project-5$ ./scheduler -h 99 tests/test1.txt -d 0
#--------------------------------------------
# Queue File               : tests/test1.txt
# Num. Disk Cylinders      : 100
# Head Position            : 99
# Head Direction           : Toward 0
#--------------------------------------------
#    405      FCFS
#    98      SSTF
#    98      SCAN
#    98      C-SCAN
#    98      LOOK
#    98      C-LOOK
ying@ubuntu:~/project5/cs441-f16-project-5$ ./scheduler -h 99 tests/test1.txt -d 1
#--------------------------------------------
# Queue File               : tests/test1.txt
# Num. Disk Cylinders      : 100
# Head Position            : 99
# Head Direction           : Toward last cylinder
#--------------------------------------------
#    405      FCFS
#    98      SSTF
#    98      SCAN
#    185      C-SCAN
#    98      LOOK
#    183      C-LOOK

3.Test boundary values of /tests/test2.txt
ying@ubuntu:~/project5/cs441-f16-project-5$ ./scheduler -h 0 tests/test2.txt -d 0
#--------------------------------------------
# Queue File               : tests/test2.txt
# Num. Disk Cylinders      : 1000
# Head Position            : 0
# Head Direction           : Toward 0
#--------------------------------------------
#    3148      FCFS
#    842      SSTF
#    842      SCAN
#    1907      C-SCAN
#    842      LOOK
#    1593      C-LOOK
ying@ubuntu:~/project5/cs441-f16-project-5$ ./scheduler -h 0 tests/test2.txt -d 1
#--------------------------------------------
# Queue File               : tests/test2.txt
# Num. Disk Cylinders      : 1000
# Head Position            : 0
# Head Direction           : Toward last cylinder
#--------------------------------------------
#    3148      FCFS
#    842      SSTF
#    842      SCAN
#    842      C-SCAN
#    842      LOOK
#    842      C-LOOK
ying@ubuntu:~/project5/cs441-f16-project-5$ ./scheduler -h 999 tests/test2.txt -d 0
#--------------------------------------------
# Queue File               : tests/test2.txt
# Num. Disk Cylinders      : 1000
# Head Position            : 999
# Head Direction           : Toward 0
#--------------------------------------------
#    3285      FCFS
#    908      SSTF
#    908      SCAN
#    908      C-SCAN
#    908      LOOK
#    908      C-LOOK
ying@ubuntu:~/project5/cs441-f16-project-5$ ./scheduler -h 999 tests/test2.txt -d 1
#--------------------------------------------
# Queue File               : tests/test2.txt
# Num. Disk Cylinders      : 1000
# Head Position            : 999
# Head Direction           : Toward last cylinder
#--------------------------------------------
#    3285      FCFS
#    908      SSTF
#    908      SCAN
#    1841      C-SCAN
#    908      LOOK
#    1659      C-LOOK
ying@ubuntu:~/project5/cs441-f16-project-5$ 

4.Test the boundary value of /tests/test3.txt
ying@ubuntu:~/project5/cs441-f16-project-5$ ./scheduler -h 0 tests/test3.txt -d 0
#--------------------------------------------
# Queue File               : tests/test3.txt
# Num. Disk Cylinders      : 50
# Head Position            : 0
# Head Direction           : Toward 0
#--------------------------------------------
#    47      FCFS
#    47      SSTF
#    47      SCAN
#    86      C-SCAN
#    47      LOOK
#    82      C-LOOK
ying@ubuntu:~/project5/cs441-f16-project-5$ ./scheduler -h 0 tests/test3.txt -d 1
#--------------------------------------------
# Queue File               : tests/test3.txt
# Num. Disk Cylinders      : 50
# Head Position            : 0
# Head Direction           : Toward last cylinder
#--------------------------------------------
#    47      FCFS
#    47      SSTF
#    47      SCAN
#    47      C-SCAN
#    47      LOOK
#    47      C-LOOK
ying@ubuntu:~/project5/cs441-f16-project-5$ ./scheduler -h 49 tests/test3.txt -d 0
#--------------------------------------------
# Queue File               : tests/test3.txt
# Num. Disk Cylinders      : 50
# Head Position            : 49
# Head Direction           : Toward 0
#--------------------------------------------
#    72      FCFS
#    37      SSTF
#    37      SCAN
#    37      C-SCAN
#    37      LOOK
#    37      C-LOOK
ying@ubuntu:~/project5/cs441-f16-project-5$ ./scheduler -h 49 tests/test3.txt -d 1
#--------------------------------------------
# Queue File               : tests/test3.txt
# Num. Disk Cylinders      : 50
# Head Position            : 49
# Head Direction           : Toward last cylinder
#--------------------------------------------
#    72      FCFS
#    37      SSTF
#    37      SCAN
#    96      C-SCAN
#    37      LOOK
#    72      C-LOOK

5. Test the head is in the middle of the requests
ying@ubuntu:~/project5/cs441-f16-project-5$ ./scheduler -h 34 tests/test1.txt -d 0
#--------------------------------------------
# Queue File               : tests/test1.txt
# Num. Disk Cylinders      : 100
# Head Position            : 34
# Head Direction           : Toward 0
#--------------------------------------------
#    340      FCFS
#    118      SSTF
#    120      SCAN
#    183      C-SCAN
#    118      LOOK
#    155      C-LOOK
ying@ubuntu:~/project5/cs441-f16-project-5$ ./scheduler -h 34 tests/test2.txt -d 0
#--------------------------------------------
# Queue File               : tests/test2.txt
# Num. Disk Cylinders      : 1000
# Head Position            : 34
# Head Direction           : Toward 0
#--------------------------------------------
#    3114      FCFS
#    808      SSTF
#    876      SCAN
#    1941      C-SCAN
#    808      LOOK
#    1559      C-LOOK
ying@ubuntu:~/project5/cs441-f16-project-5$ ./scheduler -h 34 tests/test2.txt -d 1
#--------------------------------------------
# Queue File               : tests/test2.txt
# Num. Disk Cylinders      : 1000
# Head Position            : 34
# Head Direction           : Toward last cylinder
#--------------------------------------------
#    3114      FCFS
#    808      SSTF
#    808      SCAN
#    808      C-SCAN
#    808      LOOK
#    808      C-LOOK
ying@ubuntu:~/project5/cs441-f16-project-5$ ./scheduler -h 34 tests/test3.txt -d 0
#--------------------------------------------
# Queue File               : tests/test3.txt
# Num. Disk Cylinders      : 50
# Head Position            : 34
# Head Direction           : Toward 0
#--------------------------------------------
#    57      FCFS
#    59      SSTF
#    81      SCAN
#    97      C-SCAN
#    57      LOOK
#    69      C-LOOK
ying@ubuntu:~/project5/cs441-f16-project-5$ ./scheduler -h 34 tests/test3.txt -d 1
#--------------------------------------------
# Queue File               : tests/test3.txt
# Num. Disk Cylinders      : 50
# Head Position            : 34
# Head Direction           : Toward last cylinder
#--------------------------------------------
#    57      FCFS
#    59      SSTF
#    52      SCAN
#    93      C-SCAN
#    48      LOOK
#    65      C-LOOK
ying@ubuntu:~/project5/cs441-f16-project-5$ ./scheduler -h 34 tests/test4.txt -d 0
#--------------------------------------------
# Queue File               : tests/test4.txt
# Num. Disk Cylinders      : 100
# Head Position            : 34
# Head Direction           : Toward 0
#--------------------------------------------
#    380      FCFS
#    130      SSTF
#    125      SCAN
#    196      C-SCAN
#    89      LOOK
#    144      C-LOOK
ying@ubuntu:~/project5/cs441-f16-project-5$ ./scheduler -h 34 tests/test4.txt -d 1
#--------------------------------------------
# Queue File               : tests/test4.txt
# Num. Disk Cylinders      : 100
# Head Position            : 34
# Head Direction           : Toward last cylinder
#--------------------------------------------
#    380      FCFS
#    130      SSTF
#    146      SCAN
#    182      C-SCAN
#    130      LOOK
#    130      C-LOOK
ying@ubuntu:~/project5/cs441-f16-project-5$ ./scheduler -h 34 tests/test5.txt -d 0
#--------------------------------------------
# Queue File               : tests/test5.txt
# Num. Disk Cylinders      : 200
# Head Position            : 34
# Head Direction           : Toward 0
#--------------------------------------------
#    922      FCFS
#    210      SSTF
#    224      SCAN
#    384      C-SCAN
#    210      LOOK
#    352      C-LOOK
ying@ubuntu:~/project5/cs441-f16-project-5$ ./scheduler -h 34 tests/test5.txt -d 1
#--------------------------------------------
# Queue File               : tests/test5.txt
# Num. Disk Cylinders      : 200
# Head Position            : 34
# Head Direction           : Toward last cylinder
#--------------------------------------------
#    922      FCFS
#    210      SSTF
#    357      SCAN
#    390      C-SCAN
#    339      LOOK
#    358      C-LOOK
ying@ubuntu:~/project5/cs441-f16-project-5$ 




## Known bugs and problem areas

TODO