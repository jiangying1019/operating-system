# CS441/541 Project 2 Part 1 Documentation

# Author(s): Jiang Ying

# Date: 9/20/2016

# How to build the software
  ying@ubuntu:~/Synchronization/cs441-f16-project-2/part1$ make
  gcc -o diners-v1 diners-v1.c ../lib/semaphore_support.o -Wall -g -O0 -I../lib -pthread
  gcc -o diners-v2 diners-v2.c ../lib/semaphore_support.o -Wall -g -O0 -I../lib -pthread
  ying@ubuntu:~/Synchronization/cs441-f16-project-2/part1$ ./diners-v1 2 5

# How to use the software
  ying@ubuntu:~/Synchronization/cs441-f16-project-2/part1$ make
  gcc -o diners-v1 diners-v1.c ../lib/semaphore_support.o -Wall -g -O0 -I../lib -pthread
  gcc -o diners-v2 diners-v2.c ../lib/semaphore_support.o -Wall -g -O0 -I../lib -pthread
  ying@ubuntu:~/Synchronization/cs441-f16-project-2/part1$ ./diners-v1 2 5

  ying@ubuntu:~/Synchronization/cs441-f16-project-2/part1$ ./diners-v2 2 5

# How the software was tested Test cases:
ying@ubuntu:~/Synchronization/cs441-f16-project-2/part1$ ./diners-v1 2 5
Philospher 4: Thinking!
Philospher 3: Thinking!
Philospher 2: Thinking!
Philospher 1: Thinking!
Philospher 0: Thinking!
Philospher 2: ........ Eating!
Philospher 1: ........ Eating!
Philospher 2: ........ ....... Done!!
Philospher 2: Thinking!
Philospher 0: ........ Eating!
Philospher 1: ........ ....... Done!!
Philospher 1: Thinking!
Philospher 4: ........ Eating!
Philospher 3: ........ Eating!
Philospher 0: ........ ....... Done!!
Philospher 0: Thinking!
Philospher 4: ........ ....... Done!!
Philospher 4: Thinking!
Philospher 2: ........ Eating!
......
Philospher 2: ........ ....... Done!!
Philospher 2: Thinking!
Philospher 0: ........ Eating!
Philospher 1: ........ ....... Done!!
Philospher 1: Thinking!
Philospher 4: ........ Eating!
Philospher 0: ........ ....... Done!!
Philospher 0: Thinking!
Philospher 3: ........ Eating!
Philospher 4: ........ ....... Done!!
Philospher 4: Thinking!
Philospher 2: ........ Eating!
Philospher 3: ........ ....... Done!!
Philospher 3: Thinking!
Philospher 1: ........ Eating!
Philospher 0: ........ Eating!
Philospher 2: ........ ....... Done!!
Philospher 2: Thinking!
Philospher 1: ........ ....... Done!!
Philospher 1: Thinking!
Philospher 4: ........ Eating!
Philospher 3: ........ Eating!
Philospher 0: ........ ....... Done!!

----------------------------------------
Philospher 0 Ate 110 / Thought 110
Philospher 1 Ate 110 / Thought 111
Philospher 2 Ate 109 / Thought 110
Philospher 3 Ate 108 / Thought 108
Philospher 4 Ate 110 / Thought 110


ying@ubuntu:~/Synchronization/cs441-f16-project-2/part1$ ./diners-v2 2 8
Philospher 3: Thinking!
Philospher 3: ........ Eat!
Philospher 7: Thinking!
Philospher 7: ........ Eat!
Philospher 2: Thinking!
Philospher 1: Thinking!
Philospher 1: ........ Eat!
Philospher 6: Thinking!
Philospher 5: Thinking!
Philospher 5: ........ Eat!
Philospher 4: Thinking!
Philospher 7: ........ .... Done!
Philospher 0: Thinking!
Philospher 3: ........ .... Done!
Philospher 5: ........ .... Done!
Philospher 4: ........ Eat!
Philospher 6: ........ Eat!
Philospher 4: ........ .... Done!
Philospher 1: ........ .... Done!
Philospher 0: ........ Eat!
Philospher 2: ........ Eat!
Philospher 3: Thinking!
Philospher 6: ........ .... Done!
Philospher 4: Thinking!
Philospher 4: ........ Eat!
Philospher 5: Thinking!
Philospher 7: Thinking!
Philospher 2: ........ .... Done!
Philospher 1: Thinking!
Philospher 0: ........ .... Done!
Philospher 7: ........ Eat!
Philospher 1: ........ Eat!
......
Philospher 0: Thinking!
Philospher 6: Thinking!
Philospher 1: ........ .... Done!
Philospher 5: ........ .... Done!
Philospher 7: ........ .... Done!
Philospher 6: ........ Eat!
Philospher 0: ........ Eat!
Philospher 3: ........ .... Done!
Philospher 2: ........ Eat!
Philospher 5: Thinking!
Philospher 4: Thinking!
Philospher 4: ........ Eat!
Philospher 6: ........ .... Done!
Philospher 0: ........ .... Done!
Philospher 3: Thinking!
Philospher 1: Thinking!
Philospher 7: Thinking!
Philospher 7: ........ Eat!
Philospher 2: ........ .... Done!
Philospher 1: ........ Eat!
Philospher 1: ........ .... Done!
Philospher 6: Thinking!
Philospher 2: Thinking!
Philospher 2: ........ Eat!
Philospher 0: Thinking!
Philospher 4: ........ .... Done!
Philospher 5: ........ Eat!
Philospher 5: ........ .... Done!
Philospher 7: ........ .... Done!
Philospher 6: ........ Eat!
Philospher 0: ........ Eat!
Philospher 2: ........ .... Done!
Philospher 3: ........ Eat!
Philospher 1: Thinking!

----------------------------------------
Philospher 0 Ate 130 / Thought 130
Philospher 1 Ate 126 / Thought 127
Philospher 2 Ate 136 / Thought 136
Philospher 3 Ate 130 / Thought 130
Philospher 4 Ate 135 / Thought 135
Philospher 5 Ate 133 / Thought 133
Philospher 6 Ate 131 / Thought 131
Philospher 7 Ate 130 / Thought 130

----------------------------------------
ying@ubuntu:~/Synchronization/cs441-f16-project-2/part1$ 


#Known bugs and problem areas


# How can you determine when one or more philosophers are deadlocked in your application?
  No philosopher can eat, all of them just thinkng until the program the program ends. They will wait for each other forever.
  
# How can you determine when one or more philosophers are starving in your application?
  If one or more philosopher can't eat all the time, then it is a starvation. The number of eating is obvious less than other threads if philosophers are starving.
  
# Did deadlock occur every time you ran the deadlock prone version of the solution (diner-v1)?
  No, not every time. The deadlock may possible occur.
  
# What happens when you change the number of philosophers (in both solutions) to 2? or 4? or 7? or 100?
 The latest create thread may eat less. Because createing threads take time. Actually, these thread are not create simultaneously and not race the resources simulataneously.
 In version 1.0
 The times of eating are quit different. For example: 1000 threads, thread 1 ate 80 times and thread ate 10 times. From thread 0 to 1000, the times of thread ate decrease gradually.
 In version 2.0
 Threads ate less than threads in version 1.0 and the times of each thread ate doesn't differ to much. For example: 1000 threads, thread 1 ate 20 times, thread 999 ate 14 times.

 To conclude, the deadlock version is more efficient and less fair, the deadlock free version is more fair but less efficient.

# While running the Tanenbaum solution (above in pseudo code - diner-v2) with 5 philosophers, did you notice any one philosopher periodically not consuming as much as the others?
  Try commenting out the printing of the state changing, and just look at the ﬁnal totals.
  Yes. Starvation may occur at this suitation. 

