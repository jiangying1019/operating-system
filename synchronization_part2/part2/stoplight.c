/*
 * Jiang Ying
 *
 * CS 441/541 : Project 2 Part 2 
 *
 */
#include "stoplight.h"

semaphore_t portion[4];//semaphore
int section[4] = {1,1,1,1};//portion is available or not

int main(int argc, char * argv[]) {
    int ret;

    /*
     * Parse Command Line arguments
     */
    if( 0 != (ret = parse_args(argc, argv)) ) {
        return -1;
    }
    printf("---------------------\n");
    printf("Time to live:   %d\n", ttl);
    printf("Number of cars: %d\n", num_cars);
    printf("---------------------\n");

    /*
     * Initialize:
     * - random number generator
     */
    srand(time(NULL));

    /*
     * Create Car Thread(s)
     * 
     */
    pthread_t threads[num_cars];
    semaphore_create(&mutex,1);
    semaphore_create(&mutex_count,1);
    semaphore_create(&portion[0],1);//NW
    semaphore_create(&portion[1],1);//SW
    semaphore_create(&portion[2],1);//SE
    semaphore_create(&portion[3],1);//NE
    //To granatee the order of proceeding
    semaphore_create(&north_queue, 1);
    semaphore_create(&east_queue, 1);
    semaphore_create(&south_queue, 1);
    semaphore_create(&west_queue, 1);
    
    int i,rc;
    for(i=0;i<num_cars;i++){
      rc = pthread_create(&threads[i],NULL,start_car,(void *)(intptr_t)i);
      if(0 != rc){
	printf("ERROR: return code from pthread_create() is %d\n", rc);
	exit(-1);
      }
    }

    /*
     * Wait for the TTL to expire
     */
    // sleep specified amount of time
    // set time_to_exit to TRUE
    sleep(ttl);
    time_to_exit = TRUE;

    /*
     * Reap threads
     */
    for(i=0;i<num_cars;i++){
      pthread_join(threads[i],NULL);
    }

    /*
     * Print timing information
     */
    printf("Min.    Time: %f\n", min_t);
    printf("Avg.    Time: %f\n", total_t/num_of_threads);
    printf("Max.    Time: %f\n", max_t);
    printf("Total   Time: %f\n", total_t);
    
    /*
     * Cleanup
     * 
     */
    pthread_exit(NULL);

    /*
     * Finalize support library
     */
    support_finalize();

    return 0;
}

int parse_args(int argc, char **argv)
{
    // TODO - Write me
  int ret=0;
  if(argc!=3){
    printf("Error: Requires two positive integers as parameters.\nFirst one for time the progeam runs, second one is the number of cars\n");
    return -1;
  }
  if((ret = atoi(argv[1]))!=0){
    ttl = ret;
  }else{
    printf("The first parameter is a positive integer\n");
    return -1;
  }
  if((ret = atoi(argv[2]))!=0){
    num_cars = ret;
  }else{
    printf("The second parament is a positive integer\n");
    return -1;
  }
  
    /*
     * Initialize support library
     */
    support_init();

    return 0;
}

/*
 * Approach intersection
 * param = Car Number (car_id)
 */
void *start_car(void *param) {
    int car_id = (intptr_t)param;
    car_t this_car;
    this_car.car_id = car_id;
    double spend_t;
    
    
    /*
     * Keep cycling through
     */
    while( time_to_exit == FALSE ) {
     semaphore_wait(&mutex_count);
        num_of_threads++;
      semaphore_post(&mutex_count);
        /*
         * Sleep for a bounded random amount of time before approaching the
         * intersection
         */
        usleep(random()%TIME_TO_APPROACH);
	
        /*
         * Setup the car's direction, where it is headed, set its state
         */
	this_car.appr_dir = get_random_direction(DIRMAX);
	this_car.dest_dir = get_random_direction(this_car.appr_dir);
	this_car.location = LOC_I1;
	this_car.state = STATE_WAITING_I1;


        /*
         * Mark start time for car
         */
	gettimeofday(&this_car.start_time, NULL);
        gettimeofday(&this_car.end_time, NULL);
	print_state(this_car, NULL);

	
        /*
         * Move the car in the direction and change its state accordingly
         */
	//go the intersection
	go(this_car);
	//crosse the intersection
	usleep(rand()%TIME_TO_CROSS);
	//leave the intersection
	leave(this_car);


        /*
         * Mark leave time for car
         */

       gettimeofday(&this_car.end_time, NULL);
	// print_state(this_car, NULL);

       semaphore_wait(&mutex_count);
       spend_t =get_timeval_diff_as_double(this_car.start_time, NULL)*1000;
     
       total_t = total_t+spend_t;
      
       if(num_of_threads==num_cars){
	 min_t = spend_t;
	 max_t = spend_t;
       }else{
	 if(spend_t < min_t)
	   min_t = spend_t;
	 if(spend_t > max_t)
	   max_t = spend_t;
       }
       semaphore_post(&mutex_count);
        /*
         * Save statistics about the cars travel
         */
       

    }

    /*
     * All done
     */
    pthread_exit((void *) 0);

    return NULL;
}

//print state
//test whether sections are available
void go(car_t this_car){
 
  this_car.state = STATE_APPROACH_I1;
  //print_state(this_car,NULL);

  if(this_car.appr_dir==0){
    semaphore_wait(&north_queue);
    print_state(this_car,NULL);
  }
  if(this_car.appr_dir==1){
    semaphore_wait(&west_queue);
    print_state(this_car,NULL);
  }
  if(this_car.appr_dir==2){
    semaphore_wait(&south_queue);
    print_state(this_car,NULL);
  }
  if(this_car.appr_dir==3){
    semaphore_wait(&east_queue);
    print_state(this_car,NULL);
  }
  while(test(this_car)){
       usleep(rand()%TIME_TO_CROSS);
  }
 
}
//set section to TRUE
//print leave state
void leave(car_t this_car){
  semaphore_wait(&mutex);

   if(this_car.appr_dir==0)
    semaphore_post(&north_queue);
  if(this_car.appr_dir==1)
    semaphore_post(&west_queue);
  if(this_car.appr_dir==2)
    semaphore_post(&south_queue);
  if(this_car.appr_dir==3)
    semaphore_post(&east_queue);
  
  if(getDirection(this_car)==0){
     section[this_car.appr_dir]= TRUE;
     section[(this_car.appr_dir+1)%4]= TRUE;
     section[(this_car.appr_dir+2)%4]= TRUE;

     semaphore_post(&portion[this_car.appr_dir]);
     semaphore_post(&portion[(this_car.appr_dir+1)%4]);
     semaphore_post(&portion[(this_car.appr_dir+2)%4]);
	
  }

  if(getDirection(this_car)==1){
     section[this_car.appr_dir]= TRUE;
	
     semaphore_post(&portion[this_car.appr_dir]);	
  }

  if(getDirection(this_car)==2){
      section[this_car.appr_dir] = TRUE;
      section[(this_car.appr_dir+1)%4] = TRUE;
	
      semaphore_post(&portion[this_car.appr_dir]);
      semaphore_post(&portion[(this_car.appr_dir+1)%4]);
  }

  this_car.state = STATE_LEAVE_I1;
  print_state(this_car,NULL);
  
  semaphore_post(&mutex);
}

//if the sections car need to pass are all available then lock all of them
//set section available to FALSE
//print state
//semaphore_post()
int test(car_t this_car){
  int testResult=1; //defulat value for test which dealing with no resource get 
  semaphore_wait(&mutex);
  
  if(getDirection(this_car)==0){
    //when the car turn left, it needs crosse three sections, check whether three are all avalibale
    if(section[this_car.appr_dir]&&section[(this_car.appr_dir+1)%4]&&section[(this_car.appr_dir+2)%4]){
     
      semaphore_wait(&portion[this_car.appr_dir]);
      semaphore_wait(&portion[(this_car.appr_dir+1)%4]);
      semaphore_wait(&portion[(this_car.appr_dir+2)%4]);
     
      //once the semaphore_wait the corresponded the section start update the location k 
      section[this_car.appr_dir]=FALSE;
      section[(this_car.appr_dir+1)%4]=FALSE;
      section[(this_car.appr_dir+2)%4]=FALSE;
      this_car.state = STATE_GO_LEFT_I1;
      testResult=0;
    }
  }
  if(getDirection(this_car)==1){
    if(section[this_car.appr_dir]){
     
      semaphore_wait(&portion[this_car.appr_dir]);
      
      section[this_car.appr_dir]=FALSE;
      this_car.state = STATE_GO_RIGHT_I1;
      testResult=0;
    }
  }

  if(getDirection(this_car)==2){
    if(section[this_car.appr_dir]&&section[(this_car.appr_dir+1)%4]){
      
      semaphore_wait(&portion[this_car.appr_dir]);
      semaphore_wait(&portion[(this_car.appr_dir+1)%4]);
     
      section[this_car.appr_dir] = FALSE;
      section[(this_car.appr_dir+1)%4] = FALSE;
      this_car.state = STATE_GO_STRAIGHT_I1;
      testResult=0;
    }
  }

  semaphore_post(&mutex);
  if(testResult==0){
  	print_state(this_car,NULL);
  }
  return testResult;// return the value of apply resource
}

int getDirection(car_t this_car){
  if((this_car.dest_dir+1)%4==this_car.appr_dir)
    return 0;//turn left. need lock three portions
  if((this_car.appr_dir+1)%4==this_car.dest_dir)
    return 1;//turn right need one portion
  if((this_car.appr_dir+2)%4==this_car.dest_dir)
    return 2;//go straight need two portions
  return -1;
}
