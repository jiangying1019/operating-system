# CS441/541
## Project 2 Template

This repository contains the template materials for project 2.

You may use this file for your project documentation.

- Part 1 : part1/
- Part 2 : part2/
# CS441/541 Project 2 Part 2 Documentation

# Author(s): Jiang Ying

# Date: 10/24/2016

# How to build the software
 ying@ubuntu:~/Documents/my-project-0/synchronization_part2/part2$ make
cd ../lib && make lib
make[1]: Entering directory '/home/ying/Documents/my-project-0/synchronization_part2/lib'
gcc -c -o semaphore_support.o semaphore_support.c -Wall -g
make[1]: Leaving directory '/home/ying/Documents/my-project-0/synchronization_part2/lib'
gcc -c -o support.o support.c  -Wall -g -O0 -I../lib
gcc -o stoplight stoplight.c support.o ../lib/semaphore_support.o -Wall -g -O0 -I../lib -pthread
ying@ubuntu:~/Documents/my-project-0/synchronization_part2/part2$ 


# How to use the software
 ying@ubuntu:~/Documents/my-project-0/synchronization_part2/part2$ ./stoplight 2 5
 The first argument is the time the program runs, the second number is the number of threads(cars).

# How the software was tested Test cases:
ying@ubuntu:~/Documents/my-project-0/synchronization_part2/part2$ ./stoplight 2 5
---------------------
Time to live:   2
Number of cars: 5
---------------------
      2 |   W. to N.    |  1-- |        0.018 | Waiting in line       
      2 |   W. to N.    |  1-- |        2.067 | Next in line          
      2 |   W. to N.    |  1-- |        8.175 | Turn Left             
      4 |   E. to N.    |  1-- |        0.022 | Waiting in line       
      4 |   E. to N.    |  1-- |        0.431 | Next in line          
      2 |   W. to N.    |  1-- |       29.134 | Leave                 
      4 |   E. to N.    |  1-- |       22.142 | Turn Right            
      4 |   E. to N.    |  1-- |       34.288 | Leave                 
      4 |   E. to S.    |  1-- |        0.003 | Waiting in line       
      4 |   E. to S.    |  1-- |        0.496 | Next in line          
      4 |   E. to S.    |  1-- |        6.633 | Turn Left             
      4 |   E. to S.    |  1-- |       25.674 | Leave                 
      2 |   N. to W.    |  1-- |        0.005 | Waiting in line       
      2 |   N. to W.    |  1-- |        1.953 | Next in line          
      2 |   N. to W.    |  1-- |        2.904 | Turn Right            
      2 |   N. to W.    |  1-- |       10.826 | Leave                 
      0 |   S. to W.    |  1-- |        0.023 | Waiting in line       
      0 |   S. to W.    |  1-- |        1.037 | Next in line          
      0 |   S. to W.    |  1-- |        6.962 | Turn Left             
      0 |   S. to W.    |  1-- |       14.854 | Leave                 
      0 |   N. to E.    |  1-- |        0.003 | Waiting in line       
      0 |   N. to E.    |  1-- |        1.051 | Next in line          
      0 |   N. to E.    |  1-- |        6.021 | Turn Left             
      0 |   N. to E.    |  1-- |       27.947 | Leave                 
      1 |   W. to N.    |  1-- |        0.046 | Waiting in line       
      1 |   W. to N.    |  1-- |        0.348 | Next in line          
      1 |   W. to N.    |  1-- |        4.110 | Turn Left             
      3 |   W. to N.    |  1-- |        0.028 | Waiting in line       
      1 |   W. to N.    |  1-- |       24.619 | Leave                 
      3 |   W. to N.    |  1-- |        1.621 | Next in line          
      3 |   W. to N.    |  1-- |        8.591 | Turn Left             
      1 |   N. to W.    |  1-- |        0.007 | Waiting in line       
      1 |   N. to W.    |  1-- |        0.427 | Next in line          
      1 |   N. to W.    |  1-- |        2.100 | Turn Right            
      3 |   W. to N.    |  1-- |       28.795 | Leave                 
      1 |   N. to W.    |  1-- |       14.632 | Leave                 
      4 |   W. to N.    |  1-- |        0.004 | Waiting in line       
      4 |   W. to N.    |  1-- |        0.322 | Next in line          
      4 |   W. to N.    |  1-- |        4.057 | Turn Left             
      4 |   W. to N.    |  1-- |       18.180 | Leave                 
      4 |   S. to E.    |  1-- |        0.002 | Waiting in line       
      4 |   S. to E.    |  1-- |        1.101 | Next in line          
      4 |   S. to E.    |  1-- |        5.003 | Turn Right            
      0 |   N. to W.    |  1-- |        0.006 | Waiting in line       
      0 |   N. to W.    |  1-- |        4.087 | Next in line          
      0 |   N. to W.    |  1-- |        7.002 | Turn Right            
      4 |   S. to E.    |  1-- |       19.556 | Leave                 
      0 |   N. to W.    |  1-- |       22.933 | Leave                 
      2 |   E. to W.    |  1-- |        0.004 | Waiting in line       
      2 |   E. to W.    |  1-- |        1.025 | Next in line          
      2 |   E. to W.    |  1-- |        3.113 | Go Straight           
      2 |   E. to W.    |  1-- |       24.835 | Leave                 
      1 |   S. to W.    |  1-- |        0.003 | Waiting in line       
      1 |   S. to W.    |  1-- |        1.193 | Next in line          
      1 |   S. to W.    |  1-- |        7.005 | Turn Left             
      1 |   S. to W.    |  1-- |       14.963 | Leave                 
      1 |   S. to E.    |  1-- |        0.003 | Waiting in line       
      1 |   S. to E.    |  1-- |        1.998 | Next in line          
      1 |   S. to E.    |  1-- |        4.196 | Turn Right            
      1 |   S. to E.    |  1-- |        7.044 | Leave                 
      3 |   N. to E.    |  1-- |        0.003 | Waiting in line       
      3 |   N. to E.    |  1-- |        0.597 | Next in line          
      3 |   N. to E.    |  1-- |        6.278 | Turn Left             
      3 |   N. to E.    |  1-- |       30.235 | Leave                 
      4 |   W. to S.    |  1-- |        0.004 | Waiting in line       
      4 |   W. to S.    |  1-- |        0.308 | Next in line          
      4 |   W. to S.    |  1-- |        3.098 | Turn Right            
      1 |   W. to N.    |  1-- |        0.004 | Waiting in line       
      1 |   W. to N.    |  1-- |        1.171 | Next in line          
      4 |   W. to S.    |  1-- |       18.162 | Leave                 
      1 |   W. to N.    |  1-- |       18.301 | Turn Left             
      1 |   W. to N.    |  1-- |       34.473 | Leave                 
      0 |   E. to N.    |  1-- |        0.005 | Waiting in line       
      0 |   E. to N.    |  1-- |        1.921 | Next in line          
      2 |   S. to E.    |  1-- |        0.002 | Waiting in line       
      2 |   S. to E.    |  1-- |        1.237 | Next in line          
      0 |   E. to N.    |  1-- |        4.939 | Turn Right            
      2 |   S. to E.    |  1-- |        3.148 | Turn Right            
      0 |   E. to N.    |  1-- |       19.031 | Leave                 
      2 |   S. to E.    |  1-- |       18.964 | Leave                 
      4 |   E. to S.    |  1-- |        0.004 | Waiting in line       
      4 |   E. to S.    |  1-- |        0.209 | Next in line          
      4 |   E. to S.    |  1-- |        2.915 | Turn Left             
      4 |   E. to S.    |  1-- |       22.392 | Leave                 
      3 |   N. to W.    |  1-- |        0.004 | Waiting in line       
      3 |   N. to W.    |  1-- |        0.589 | Next in line          
      3 |   N. to W.    |  1-- |        3.597 | Turn Right            
      3 |   N. to W.    |  1-- |       26.510 | Leave                 
      4 |   S. to N.    |  1-- |        0.004 | Waiting in line       
      1 |   E. to S.    |  1-- |        0.002 | Waiting in line       
      4 |   S. to N.    |  1-- |        0.388 | Next in line          
      1 |   E. to S.    |  1-- |        0.368 | Next in line          
      4 |   S. to N.    |  1-- |        2.088 | Go Straight           
      4 |   S. to N.    |  1-- |       14.502 | Leave                 
      1 |   E. to S.    |  1-- |       17.874 | Turn Left             
      1 |   E. to S.    |  1-- |       24.914 | Leave                 
      0 |   S. to W.    |  1-- |        0.004 | Waiting in line       
      0 |   S. to W.    |  1-- |        0.660 | Next in line          
      0 |   S. to W.    |  1-- |        3.485 | Turn Left             
      0 |   S. to W.    |  1-- |       25.097 | Leave                 
      2 |   S. to W.    |  1-- |        0.009 | Waiting in line       
      2 |   S. to W.    |  1-- |        0.576 | Next in line          
      2 |   S. to W.    |  1-- |        2.885 | Turn Left             
      2 |   S. to W.    |  1-- |       25.913 | Leave                 
      3 |   N. to E.    |  1-- |        0.002 | Waiting in line       
      3 |   N. to E.    |  1-- |        1.067 | Next in line          
      3 |   N. to E.    |  1-- |        6.943 | Turn Left             
      3 |   N. to E.    |  1-- |       29.673 | Leave                 
      4 |   W. to E.    |  1-- |        0.003 | Waiting in line       
      4 |   W. to E.    |  1-- |        1.479 | Next in line          
      4 |   W. to E.    |  1-- |        7.524 | Go Straight           
      4 |   W. to E.    |  1-- |       27.647 | Leave                 
      1 |   E. to N.    |  1-- |        0.003 | Waiting in line       
      1 |   E. to N.    |  1-- |        1.073 | Next in line          
      1 |   E. to N.    |  1-- |        4.986 | Turn Right            
      1 |   E. to N.    |  1-- |       21.034 | Leave                 
      3 |   S. to W.    |  1-- |        0.003 | Waiting in line       
      3 |   S. to W.    |  1-- |        2.233 | Next in line          
      3 |   S. to W.    |  1-- |        7.965 | Turn Left             
      0 |   S. to N.    |  1-- |        0.002 | Waiting in line       
      0 |   S. to N.    |  1-- |        1.009 | Next in line          
      3 |   S. to W.    |  1-- |       13.196 | Leave                 
      0 |   S. to N.    |  1-- |        6.049 | Go Straight           
      0 |   S. to N.    |  1-- |       28.207 | Leave                 
      2 |   W. to N.    |  1-- |        0.003 | Waiting in line       
      2 |   W. to N.    |  1-- |        0.966 | Next in line          
      4 |   S. to E.    |  1-- |        0.003 | Waiting in line       
      4 |   S. to E.    |  1-- |        0.297 | Next in line          
      2 |   W. to N.    |  1-- |        4.966 | Turn Left             
      2 |   W. to N.    |  1-- |       20.047 | Leave                 
      4 |   S. to E.    |  1-- |       18.799 | Turn Right            
      4 |   S. to E.    |  1-- |       25.639 | Leave                 
      3 |   E. to W.    |  1-- |        0.004 | Waiting in line       
      3 |   E. to W.    |  1-- |        1.761 | Next in line          
      3 |   E. to W.    |  1-- |        6.596 | Go Straight           
      3 |   E. to W.    |  1-- |       13.196 | Leave                 
      0 |   E. to S.    |  1-- |        0.003 | Waiting in line       
      0 |   E. to S.    |  1-- |        1.235 | Next in line          
      0 |   E. to S.    |  1-- |        7.976 | Turn Left             
      0 |   E. to S.    |  1-- |       29.047 | Leave                 
      1 |   E. to W.    |  1-- |        0.008 | Waiting in line       
      1 |   E. to W.    |  1-- |        0.446 | Next in line          
      1 |   E. to W.    |  1-- |        2.168 | Go Straight           
      1 |   E. to W.    |  1-- |       20.655 | Leave                 
Min.    Time: 9.126991
Avg.    Time: 29.356487
Max.    Time: 38.551961
Total   Time: 910.051090
ying@ubuntu:~/Documents/my-project-0/synchronization_part2/part2$ 



ying@ubuntu:~/Documents/my-project-0/synchronization_part2/part2$ ./stoplight 10 100
---------------------
Time to live:   10
Number of cars: 100
---------------------
     89 |   W. to E.    |  1-- |        0.032 | Waiting in line       
     89 |   W. to E.    |  1-- |        1.976 | Next in line          
     82 |   N. to W.    |  1-- |        0.030 | Waiting in line       
      6 |   E. to W.    |  1-- |        0.021 | Waiting in line       
     34 |   N. to S.    |  1-- |        0.014 | Waiting in line       
     82 |   N. to W.    |  1-- |        3.461 | Next in line          
      6 |   E. to W.    |  1-- |        1.609 | Next in line          
     34 |   N. to S.    |  1-- |        1.589 | Next in line          
     49 |   W. to E.    |  1-- |        0.021 | Waiting in line       
     89 |   W. to E.    |  1-- |        9.829 | Go Straight           
     49 |   W. to E.    |  1-- |        0.365 | Next in line          
     82 |   N. to W.    |  1-- |        5.819 | Turn Right            
     36 |   N. to S.    |  1-- |        0.032 | Waiting in line       
     36 |   N. to S.    |  1-- |        0.437 | Next in line          
     82 |   N. to W.    |  1-- |        9.094 | Leave                 
     47 |   E. to W.    |  1-- |        0.034 | Waiting in line       
     89 |   W. to E.    |  1-- |       20.174 | Leave                 
     47 |   E. to W.    |  1-- |        0.998 | Next in line          
     20 |   N. to S.    |  1-- |        0.022 | Waiting in line       
     62 |   S. to W.    |  1-- |        0.059 | Waiting in line       
      1 |   S. to W.    |  1-- |        0.002 | Waiting in line       
     20 |   N. to S.    |  1-- |        1.930 | Next in line          
     62 |   S. to W.    |  1-- |        6.367 | Next in line          
      1 |   S. to W.    |  1-- |        6.590 | Next in line          
     47 |   E. to W.    |  1-- |       10.049 | Go Straight           
     49 |   W. to E.    |  1-- |       21.065 | Go Straight           
     13 |   N. to W.    |  1-- |        0.007 | Waiting in line       
     13 |   N. to W.    |  1-- |        0.343 | Next in line          
     69 |   W. to E.    |  1-- |        0.004 | Waiting in line       
     69 |   W. to E.    |  1-- |        1.199 | Next in line          
     56 |   S. to E.    |  1-- |        0.004 | Waiting in line       
     56 |   S. to E.    |  1-- |        1.054 | Next in line          
     60 |   S. to E.    |  1-- |        0.007 | Waiting in line       
     47 |   E. to W.    |  1-- |       32.644 | Leave                 
     60 |   S. to E.    |  1-- |        0.262 | Next in line          
     49 |   W. to E.    |  1-- |       45.026 | Leave                 
     91 |   N. to W.    |  1-- |        0.001 | Waiting in line       
     91 |   N. to W.    |  1-- |        1.175 | Next in line          
     41 |   E. to N.    |  1-- |        0.003 | Waiting in line
     ......


      57 |   S. to W.    |  1-- |     1134.058 | Turn Left             
     57 |   S. to W.    |  1-- |     1156.638 | Leave                 
     48 |   W. to N.    |  1-- |     3460.575 | Turn Left             
     48 |   W. to N.    |  1-- |     3471.037 | Leave                 
      7 |   S. to W.    |  1-- |     2128.807 | Turn Left             
      7 |   S. to W.    |  1-- |     2131.774 | Leave                 
     97 |   W. to E.    |  1-- |     1265.850 | Go Straight           
     97 |   W. to E.    |  1-- |     1285.222 | Leave                 
     32 |   E. to S.    |  1-- |      997.405 | Turn Left             
     32 |   E. to S.    |  1-- |     1010.413 | Leave                 
     63 |   W. to N.    |  1-- |      664.359 | Turn Left             
     63 |   W. to N.    |  1-- |      674.491 | Leave                 
     67 |   W. to N.    |  1-- |     2333.317 | Turn Left             
     67 |   W. to N.    |  1-- |     2346.420 | Leave                 
     62 |   S. to W.    |  1-- |     1911.602 | Turn Left             
     62 |   S. to W.    |  1-- |     1916.111 | Leave                 
     56 |   W. to N.    |  1-- |     8800.925 | Turn Left             
     56 |   W. to N.    |  1-- |     8810.864 | Leave                 
Min.    Time: 4.593995
Avg.    Time: 645.850474
Max.    Time: 8811.494189
Total   Time: 740790.494210
ying@ubuntu:~/Documents/my-project-0/synchronization_part2/part2$ 


#Known bugs and problem areas


My solution to the code:
The intersection is divided into four protions. Turn left needs three portion, go straight needs two portions, turn right needs one portion.
Before a car enters the intersection, it tests whether the protions it needs are available or not.
If all the portions a car needs to cross the intersection are available, it lock all the portions it needs. When it leave the intersection, it release all the portions.

How does your solution avoid deadlock?
The intersection is divided into four portions. The car going to different destination will pass different portions.
If the car is ready to enter the intersection, it will test all the portion it needs first.
If all the portions it needs are availbale, it will lock all the portions and crosse the intersection.
Otherwise, it will give up locking any portion(s).
When the car leaves the intersection, it release the locks.
It breaks the deadlock condition hold and wait.

How does your solution prevent accidents?
Every time the car wants to enter the intersection, it test whether the portions it needs are available.
If the portions are available, then it can pass the intersection. It means it is impossiable that another car can enter the portion the car locked.
No more than two cars in the same portion means no accident.

How does your solution improve traffic ﬂow?
Turn left needs three portions
Turn right needs one portion
Go straight needs two portions
Cars don't lock the entire intersection, they lock the portions they need.
It is possible that four cars turn right at the same time, two cars go straight at the same time, one car turn left and one car turn right at the same time.

How does your solution preserve the order of cars approaching the intersection
There are four semaphores north_queue, west_queue, south_queue, and east_queue. Each entry of the intersection has a semaphore.
A car in one of the entries has to wait for the queue semaphore. When the car leave, the car increase the semaphore and the car behind it can enter.

How is your solution “fair”?
When a car can't enter the intersection, it will sleep for a while and test whether it can go or not.
It will keep testing until it enters the intersection and leave.
First approching car leaves first.

How does your solution prevent “starvation”?
The car is aproching the intersection, and no portion is available, then it will test whether the portion is available.
Once it is available, it enters the intersection. So it will enter the intersection finally.


- Bonus : bonus/

