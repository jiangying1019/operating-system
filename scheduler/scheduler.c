/*
 * Author: Jiang Ying
 *
 * CS 441/541: CPU Scheduler (Project 4)
 */

#include "scheduler.h"
int scheduler = 0;
int quantum = 0;
int num_of_procs = 0;
char *file_name = NULL;
struct process *proc = NULL;

void print_head(){
  switch(scheduler){
  case 1:
    printf("Scheduler     : 1 FCFS\n");
    break;
  case 2:
    printf("Scheduler     : 2 SJF\n");
  case 3:
    printf("Scheduler     : 3 Priority\n");
  case 4:
    printf("Scheduler     : 4 RR\n");
  default: ;
  }
    printf("Quantum       : %d\n", quantum);
    printf("Sch.file      : %s\n", file_name);
    printf("---------------------------------------------\n");
}

int main(int argc, char **argv) {
  
  parse_argus(argc, argv);
  print_head();
  
  initial_processes(&proc);

  if(scheduler==1){
     fcfs();
  }
  if(scheduler==2){
    sjf();
  }
  if(scheduler==3){
    priority();
  }
  if(scheduler==4){
    rr();
  }
  print_procs();
    if(proc!=NULL){
      struct process *temp = proc;
      for(;temp!=NULL;temp = temp->next){
	if(temp->next!=NULL){
	  free(temp->next);
	  temp->next = NULL;
	}
      }
      free(proc);
      proc = NULL;
      }
    return 0;
}

void parse_argus(int args, char **argv){
  int i=1, qflag = 0, sflag = 0;
 
  while(i<args){
    if(strcmp(argv[i], "-s")==0){
      if(i+1<args){
	 if((scheduler=atoi(argv[i+1]))!=0){
	  i = i+2;
	  sflag = 1;
	    if(scheduler>4||scheduler<0){
	      printf("-s needs an integer from 1 to 4\n");
	      exit(-1);
	    }
	  }
	 else{
	   printf("-s requires a positive number as argument\n");
	   exit(-1);
	 }
      }
      else{
	printf("-s requires a positive number as argument\n");
	exit(-1);
      }
    }
   else if(strcmp(argv[i], "-q")==0){
     if(i+1<args){
        if((quantum=atoi(argv[i+1]))!=0){
	    i = i+2;
	    qflag = 1;
	 }else{
	  printf("-q requires a positive number as argument\n");
	  exit(-1);
	} 
    }
    else{
       printf("-q requires a positive number as argument\n");
       exit(-1);
	}
   }
   else if(file_name==NULL){
      file_name = argv[i];
      i = i+1;
    }
   else i = i+1;
  }
 
  if(sflag==0){
    printf("-s is required argument\n");
    exit(-1);
  }
  if(scheduler==4&&qflag==0){
    printf("Round-Robin needs a quantum\n");
    exit(-1);
  }

   if(file_name==NULL){
    printf("Missing filename\n");
    exit(-1);
    }
}

void create_processes(struct process **proc,int pid, int burst, int priority){
 struct process *pre = *proc;
 struct process *temp;
 if((*proc)==NULL){
    (*proc)=(struct process*)malloc(sizeof(struct process));
    (*proc)->pid=pid;
    (*proc)->burst=burst;
    (*proc)->remain_time=burst;
    (*proc)->priority=priority;
    (*proc)->next=NULL;
    return;
  }
  temp=pre->next;
  while(temp){
    pre=temp;
    temp=pre->next;
  }

  temp=(struct process *)malloc(sizeof(struct process));
  temp->pid=pid;
  temp->burst=burst;
  temp->priority=priority;
  temp->remain_time=burst;
  pre->next=temp;
  temp->next = NULL;
}

void initial_processes(struct process **proc){
    FILE *fd = NULL;
    char *fgets_rtn = NULL;
    char line[ LINELEN ];
    int pid = 0;
    int burst = 0;
    int priority = 0;
    int i = 1;  
    
    // Open the file

    fd = fopen(file_name, "r");
    // Keep reading until the end-of-file marker
    printf("Arrival Order: \n");
    while( 0 == feof( fd ) ) {
      // Read characters into the buffer
      //deal with the fist line, the number of processes
      if(i==1){
	 fgets_rtn = fgets( line, LINELEN, fd);
        if( NULL == fgets_rtn ) {
            break;
        }                                                                                                                                                                         
        if( '\n' == line[ strlen(line) -1 ] ) {
            line[ strlen(line) -1 ] = '\0';
        }
      
	num_of_procs = atoi(line);
      }
      //else deal with the process information
      else{
	fgets_rtn = fgets( line, LINELEN, fd);
        if( NULL == fgets_rtn ) {
            break;
        }
                                                                                                                                                                           
        if( '\n' == line[ strlen(line) -1 ] ) {
            line[ strlen(line) -1 ] = '\0';
        }

	process_line(line, &pid, &burst, &priority);
	create_processes(proc, pid, burst, priority);
	printf("%d, ",pid);
       
      }
      i = i+1;
      }
    printf("\n");
    // Close the file                                                                                                                                                                                      
    fclose(fd);
  printf("Processing information:\n");
  struct process *temp = *proc;
  if(proc==NULL);
  else{
    while(temp!=NULL){
      printf("%d\t%d\t%d\n",temp->pid, temp->burst, temp->priority);
      temp=temp->next;
      }
    }
  printf("-------------------------------------------------------\n");
    fflush(NULL);
}

void process_line(char *line, int *pid, int *burst, int *priority){
  char *copy = strdup(line);
  char *token;
  token = strtok(copy, " ");
  *pid = atoi(token);
  token = strtok(NULL, " ");
  *burst = atoi(token);
  token = strtok(NULL, " ");
  *priority = atoi(token);
}

void fcfs(){
  int wait_time = 0, start = 0, turnaround_time = 0;
  struct process *temp = proc;
  if(proc==NULL);
  else{
    while(temp!=NULL){
      wait_time = start;
      turnaround_time = temp->burst + wait_time;
      temp->waiting_time = wait_time;
      temp->turnaround_time = turnaround_time;
      start = start+temp->burst;
      temp=temp->next;
      }
    }
    fflush(NULL);
}

void sjf(){
  struct process *proctemp = proc;
  struct process *copy = NULL;
  initial_processes(&copy);
  MergeSort(&copy,2);
 
 //calculate the wait time and turnaround time of the copy list
  int wait_time = 0, start = 0, turnaround_time = 0;
  struct process *temp = copy;
  if(copy==NULL);
  else{
    while(temp!=NULL){
      wait_time = start;
      turnaround_time = temp->burst + wait_time;
      temp->waiting_time = wait_time;
      temp->turnaround_time = turnaround_time;
      start = start+temp->burst;
      temp=temp->next;
      }
    }
    
    temp = copy;
  if(copy==NULL);
  else{
    while(temp!=NULL){
      while(proctemp!=NULL){
	if(temp->pid == proctemp->pid){
	 proctemp->waiting_time= temp->waiting_time;
	 proctemp->turnaround_time = temp->turnaround_time;
	}
	proctemp = proctemp->next;
      }
      proctemp = proc;
      temp = temp->next;
    }
  }

 //free copy
     if(copy!=NULL){
      struct process *temp = copy;
      for(;temp!=NULL;temp = temp->next){
	if(temp->next!=NULL){
	  free(temp->next);
	  temp->next = NULL;
	}
      }
      free(copy);
      copy = NULL;
      }
}

void priority(){
  struct process *proctemp = proc;
  struct process *copy = NULL;
  initial_processes(&copy);
  MergeSort(&copy, 3);
 
 //calculate the wait time and turnaround time of the copy list
  int wait_time = 0, start = 0, turnaround_time = 0;
  struct process *temp = copy;
  if(copy==NULL);
  else{
    while(temp!=NULL){
      wait_time = start;
      turnaround_time = temp->burst + wait_time;
      temp->waiting_time = wait_time;
      temp->turnaround_time = turnaround_time;
      start = start+temp->burst;
      temp=temp->next;
      }
    }

    temp = copy;
  if(copy==NULL);
  else{
    while(temp!=NULL){
      while(proctemp!=NULL){
	if(temp->pid == proctemp->pid){
	 proctemp->waiting_time= temp->waiting_time;
	 proctemp->turnaround_time = temp->turnaround_time;
	}
	proctemp = proctemp->next;
      }
      proctemp = proc;
      temp = temp->next;
    }
  }

 //free copy
     if(copy!=NULL){
      struct process *temp = copy;
      for(;temp!=NULL;temp = temp->next){
	if(temp->next!=NULL){
	  free(temp->next);
	  temp->next = NULL;
	}
      }
      free(copy);
      copy = NULL;
      }
}

void rr(){
  int total=0, flag = 0, count = num_of_procs;
  struct process *temp = proc;

  if(proc==NULL);
  else{
     while(count != 0){
       while(temp!=NULL){
  
      if(temp->remain_time <= quantum && temp->remain_time >0){
	total = total + temp->remain_time;
	temp->remain_time = 0;
	flag = 1;
      }
      if(temp->remain_time > 0){
	total = total + quantum;
	temp->remain_time = temp->remain_time - quantum;
      }
      
      if(temp->remain_time == 0 && flag == 1){
	count --;
	temp->waiting_time = total - temp->burst;
	temp->turnaround_time = total;
	flag = 0;
      }
      temp=temp->next;
      }
       temp = proc;    
      }
    fflush(NULL);
     }
}

void print_procs(){
 struct process *temp = proc;
 double total_waiting_time = 0;
 double total_turnaround_time = 0;
  if(proc==NULL);
  else{
    printf("Running...\n");
    printf("##############################################\n");
    printf("# #\tCUP\tPri\tW\tT\n");
    
    while(temp!=NULL){
      printf("# %d\t%d\t%d\t%d\t%d\t\n",temp->pid, temp->burst, temp->priority, temp->waiting_time, temp->turnaround_time);
      total_turnaround_time = temp->turnaround_time +total_turnaround_time;
      total_waiting_time = total_waiting_time+temp->waiting_time;
      temp=temp->next;
      }
    }
   printf("##############################################\n");
   printf("# Avg. Waiting Time:       :%.2f\n", total_waiting_time/num_of_procs);
   printf("# Avg. Turnaround Time:    :%.2f\n", total_turnaround_time/num_of_procs);
   printf("##############################################\n");
   fflush(NULL);
}


void MergeSort(struct process** headRef, int type)
{
  struct process* head = *headRef;
  struct process* a;
  struct process* b;

/* Base case -- length 0 or 1 */
  if ((head == NULL) || (head->next == NULL))
    {
	return;
    }

/* Split head into 'a' and 'b' sublists */
  FrontBackSplit(head, &a, &b); 

/* Recursively sort the sublists */
  MergeSort(&a,type);
  MergeSort(&b,type);

/* answer = merge the two sorted lists together */
  *headRef = SortedMerge(a, b,type);
}


struct process* SortedMerge(struct process* a, struct process* b, int type){
  struct process* result = NULL;

/* Base cases */
  if (a == NULL)
	return(b);
  else if (b==NULL)
	return(a);

/* Pick either a or b, and recur */
  if(type == 2){
   if (a->burst <= b->burst){
	result = a;
	result->next = SortedMerge(a->next, b, type);
     }
   else{
	result = b;
	result->next = SortedMerge(a, b->next,type);
     }
  }

  if(type == 3){
   if (a->priority <= b->priority){
     result = a;
	result->next = SortedMerge(a->next, b, type);
     }
   else{
	result = b;
	result->next = SortedMerge(a, b->next, type);
     }
 }
return(result);
}

void FrontBackSplit(struct process* source,struct process** frontRef, struct process** backRef){
  struct process* fast;
  struct process* slow;
  if (source==NULL || source->next==NULL){
	/* length < 2 cases */
	*frontRef = source;
	*backRef = NULL;
    }
  else
    {
	slow = source;
	fast = source->next;

	/* Advance 'fast' two nodes, and advance 'slow' one node */
	while (fast != NULL){
	fast = fast->next;
	if (fast != NULL){
		slow = slow->next;
		fast = fast->next;
	  }
	}

	/* 'slow' is before the midpoint in the list, so split it in two
	at that point. */
	*frontRef = source;
	*backRef = slow->next;
	slow->next = NULL;
    }
}
