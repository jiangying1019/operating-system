/*
 * 
 *
 * CS 441/541: CPU Scheduler (Project 4)
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>


/******************************
 * Defines
 ******************************/


/******************************
 * Structures
 ******************************/
struct process{
  int pid;
  int burst;
  int remain_time;
  int priority;
  int waiting_time;
  int turnaround_time;
  struct process *next; 
};

/******************************
 * Global Variables
 ******************************/
#define LINELEN 512


/******************************
 * Function declarations
 ******************************/
//parse arguements
void parse_argus(int args, char **argv);

//create and initial processes. give each line value to a process 
void initial_processes(struct process **proc);

//process line
void process_line(char *line, int *pid, int *burst, int *priority);

//Create process
void create_processes(struct process **proc,int pid, int burst, int priority);

//fist come first serve
void fcfs();

//short job first
void sjf();

//priority
void priority();

//round robin
void rr();

//print the result
void print_procs();

//merge sort functions
struct process* SortedMerge(struct process* a, struct process* b, int type);
void FrontBackSplit(struct process* source,struct process** frontRef, struct process** backRef);
void MergeSort(struct process** headRef, int type);


