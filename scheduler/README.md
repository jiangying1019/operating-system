# CS441/541 Project 4

## Author(s):

Jiang Ying


## Date:

11/5/2016


## Description:

CPU Scheduling Simulator 


## How to build the software

ying@ubuntu:~/project3/cs441-f16-project-3$ make
gcc -o scheduler -Wall -g -O0 scheduler.c
ying@ubuntu:~/project3/cs441-f16-project-3$ 



## How to use the software
 Required: -s # Scheduling algorithm to use identiﬁed by number. The table below describes the association of parameters to scheduling algorithms.
 -s 1 First-Come, First-Served (FCFS)
 -s 2 Shortest-Job First (SJF)
 -s 3 Priority
 -s 4 Round-Robin (RR)
 • Required for RR only:
 -q # The quantum to use in the Round-Robin scheduling algorithm. This option is not required for the other scheduling algorithms, and, if provided for those algorithms, is ignored.

for example:
ying@ubuntu:~/project3/cs441-f16-project-3$ ./scheduler -s 4 tests/test5.txt -q 3



## How the software was tested
1. tests/test1.txt
Testing the position of "-s" and the processes' id are not consistent.
Testing four alogrithms.

ying@ubuntu:~/project3/cs441-f16-project-3$ ./scheduler tests/test1.txt -s 1
Scheduler     : 1 FCFS
Quantum       : 0
Sch.file      : tests/test1.txt
---------------------------------------------
Arrival Order: 
22, 38, 10, 21, 4, 9, 7, 13, 2, 5, 
Processing information:
22	2	4
38	7	8
10	5	1
21	2	3
4	3	2
9	4	5
7	6	6
13	8	9
2	1	7
5	5	4
-------------------------------------------------------
Running...
##############################################
# #	CUP	Pri	W	T
# 22	2	4	0	2	
# 38	7	8	2	9	
# 10	5	1	9	14	
# 21	2	3	14	16	
# 4	3	2	16	19	
# 9	4	5	19	23	
# 7	6	6	23	29	
# 13	8	9	29	37	
# 2	1	7	37	38	
# 5	5	4	38	43	
##############################################
# Avg. Waiting Time:       :18.70
# Avg. Turnaround Time:    :23.00
##############################################
ying@ubuntu:~/project3/cs441-f16-project-3$ ./scheduler tests/test1.txt -s 2
Scheduler     : 2 SJF
Scheduler     : 3 Priority
Scheduler     : 4 RR
Quantum       : 0
Sch.file      : tests/test1.txt
---------------------------------------------
Arrival Order: 
22, 38, 10, 21, 4, 9, 7, 13, 2, 5, 
Processing information:
22	2	4
38	7	8
10	5	1
21	2	3
4	3	2
9	4	5
7	6	6
13	8	9
2	1	7
5	5	4
-------------------------------------------------------
Arrival Order: 
22, 38, 10, 21, 4, 9, 7, 13, 2, 5, 
Processing information:
22	2	4
38	7	8
10	5	1
21	2	3
4	3	2
9	4	5
7	6	6
13	8	9
2	1	7
5	5	4
-------------------------------------------------------
Running...
##############################################
# #	CUP	Pri	W	T
# 22	2	4	1	3	
# 38	7	8	28	35	
# 10	5	1	12	17	
# 21	2	3	3	5	
# 4	3	2	5	8	
# 9	4	5	8	12	
# 7	6	6	22	28	
# 13	8	9	35	43	
# 2	1	7	0	1	
# 5	5	4	17	22	
##############################################
# Avg. Waiting Time:       :13.10
# Avg. Turnaround Time:    :17.40
##############################################
ying@ubuntu:~/project3/cs441-f16-project-3$ ./scheduler tests/test1.txt -s 3
Scheduler     : 3 Priority
Scheduler     : 4 RR
Quantum       : 0
Sch.file      : tests/test1.txt
---------------------------------------------
Arrival Order: 
22, 38, 10, 21, 4, 9, 7, 13, 2, 5, 
Processing information:
22	2	4
38	7	8
10	5	1
21	2	3
4	3	2
9	4	5
7	6	6
13	8	9
2	1	7
5	5	4
-------------------------------------------------------
Arrival Order: 
22, 38, 10, 21, 4, 9, 7, 13, 2, 5, 
Processing information:
22	2	4
38	7	8
10	5	1
21	2	3
4	3	2
9	4	5
7	6	6
13	8	9
2	1	7
5	5	4
-------------------------------------------------------
Running...
##############################################
# #	CUP	Pri	W	T
# 22	2	4	10	12	
# 38	7	8	28	35	
# 10	5	1	0	5	
# 21	2	3	8	10	
# 4	3	2	5	8	
# 9	4	5	17	21	
# 7	6	6	21	27	
# 13	8	9	35	43	
# 2	1	7	27	28	
# 5	5	4	12	17	
##############################################
# Avg. Waiting Time:       :16.30
# Avg. Turnaround Time:    :20.60
##############################################
ying@ubuntu:~/project3/cs441-f16-project-3$ ./scheduler tests/test1.txt -s 3 -q 2
Scheduler     : 3 Priority
Scheduler     : 4 RR
Quantum       : 2
Sch.file      : tests/test1.txt
---------------------------------------------
Arrival Order: 
22, 38, 10, 21, 4, 9, 7, 13, 2, 5, 
Processing information:
22	2	4
38	7	8
10	5	1
21	2	3
4	3	2
9	4	5
7	6	6
13	8	9
2	1	7
5	5	4
-------------------------------------------------------
Arrival Order: 
22, 38, 10, 21, 4, 9, 7, 13, 2, 5, 
Processing information:
22	2	4
38	7	8
10	5	1
21	2	3
4	3	2
9	4	5
7	6	6
13	8	9
2	1	7
5	5	4
-------------------------------------------------------
Running...
##############################################
# #	CUP	Pri	W	T
# 22	2	4	10	12	
# 38	7	8	28	35	
# 10	5	1	0	5	
# 21	2	3	8	10	
# 4	3	2	5	8	
# 9	4	5	17	21	
# 7	6	6	21	27	
# 13	8	9	35	43	
# 2	1	7	27	28	
# 5	5	4	12	17	
##############################################
# Avg. Waiting Time:       :16.30
# Avg. Turnaround Time:    :20.60
##############################################


2.tests/test2.txt
Testing the reverse pid order. Testing "-s" and "-q" position. Testing when choose RR, missing quantum. Test four algorithms.
ying@ubuntu:~/project3/cs441-f16-project-3$ ./scheduler tests/test2.txt -s 1
Scheduler     : 1 FCFS
Quantum       : 0
Sch.file      : tests/test2.txt
---------------------------------------------
Arrival Order: 
5, 4, 3, 2, 1, 
Processing information:
5	5	5
4	4	4
3	3	3
2	2	2
1	1	1
-------------------------------------------------------
Running...
##############################################
# #	CUP	Pri	W	T
# 5	5	5	0	5	
# 4	4	4	5	9	
# 3	3	3	9	12	
# 2	2	2	12	14	
# 1	1	1	14	15	
##############################################
# Avg. Waiting Time:       :8.00
# Avg. Turnaround Time:    :11.00
##############################################
ying@ubuntu:~/project3/cs441-f16-project-3$ ./scheduler -s 2 tests/test2.txt 
Scheduler     : 2 SJF
Scheduler     : 3 Priority
Scheduler     : 4 RR
Quantum       : 0
Sch.file      : tests/test2.txt
---------------------------------------------
Arrival Order: 
5, 4, 3, 2, 1, 
Processing information:
5	5	5
4	4	4
3	3	3
2	2	2
1	1	1
-------------------------------------------------------
Arrival Order: 
5, 4, 3, 2, 1, 
Processing information:
5	5	5
4	4	4
3	3	3
2	2	2
1	1	1
-------------------------------------------------------
Running...
##############################################
# #	CUP	Pri	W	T
# 5	5	5	10	15	
# 4	4	4	6	10	
# 3	3	3	3	6	
# 2	2	2	1	3	
# 1	1	1	0	1	
##############################################
# Avg. Waiting Time:       :4.00
# Avg. Turnaround Time:    :7.00
##############################################
ying@ubuntu:~/project3/cs441-f16-project-3$ ./scheduler -s 3 tests/test2.txt 
Scheduler     : 3 Priority
Scheduler     : 4 RR
Quantum       : 0
Sch.file      : tests/test2.txt
---------------------------------------------
Arrival Order: 
5, 4, 3, 2, 1, 
Processing information:
5	5	5
4	4	4
3	3	3
2	2	2
1	1	1
-------------------------------------------------------
Arrival Order: 
5, 4, 3, 2, 1, 
Processing information:
5	5	5
4	4	4
3	3	3
2	2	2
1	1	1
-------------------------------------------------------
Running...
##############################################
# #	CUP	Pri	W	T
# 5	5	5	10	15	
# 4	4	4	6	10	
# 3	3	3	3	6	
# 2	2	2	1	3	
# 1	1	1	0	1	
##############################################
# Avg. Waiting Time:       :4.00
# Avg. Turnaround Time:    :7.00
##############################################
ying@ubuntu:~/project3/cs441-f16-project-3$ ./scheduler -s 4 tests/test2.txt 
Round-Robin needs a quantum
ying@ubuntu:~/project3/cs441-f16-project-3$ ./scheduler -s 4 tests/test2.txt -q 2
Scheduler     : 4 RR
Quantum       : 2
Sch.file      : tests/test2.txt
---------------------------------------------
Arrival Order: 
5, 4, 3, 2, 1, 
Processing information:
5	5	5
4	4	4
3	3	3
2	2	2
1	1	1
-------------------------------------------------------
Running...
##############################################
# #	CUP	Pri	W	T
# 5	5	5	10	15	
# 4	4	4	9	13	
# 3	3	3	11	14	
# 2	2	2	6	8	
# 1	1	1	8	9	
##############################################
# Avg. Waiting Time:       :8.80
# Avg. Turnaround Time:    :11.80
##############################################
ying@ubuntu:~/project3/cs441-f16-project-3$

3. tests/test3.txt
Testing the priority in ascend order and CUP burst in descend order.
Tetsing four algorithm
ying@ubuntu:~/project3/cs441-f16-project-3$ ./scheduler -s 1 tests/test3.txt
Scheduler     : 1 FCFS
Quantum       : 0
Sch.file      : tests/test3.txt
---------------------------------------------
Arrival Order: 
4, 7, 2, 8, 6, 9, 3, 5, 
Processing information:
4	8	1
7	7	2
2	6	3
8	5	4
6	4	5
9	3	6
3	2	7
5	1	8
-------------------------------------------------------
Running...
##############################################
# #	CUP	Pri	W	T
# 4	8	1	0	8	
# 7	7	2	8	15	
# 2	6	3	15	21	
# 8	5	4	21	26	
# 6	4	5	26	30	
# 9	3	6	30	33	
# 3	2	7	33	35	
# 5	1	8	35	36	
##############################################
# Avg. Waiting Time:       :21.00
# Avg. Turnaround Time:    :25.50
##############################################
ying@ubuntu:~/project3/cs441-f16-project-3$ ./scheduler -s 2 tests/test3.txt
Scheduler     : 2 SJF
Scheduler     : 3 Priority
Scheduler     : 4 RR
Quantum       : 0
Sch.file      : tests/test3.txt
---------------------------------------------
Arrival Order: 
4, 7, 2, 8, 6, 9, 3, 5, 
Processing information:
4	8	1
7	7	2
2	6	3
8	5	4
6	4	5
9	3	6
3	2	7
5	1	8
-------------------------------------------------------
Arrival Order: 
4, 7, 2, 8, 6, 9, 3, 5, 
Processing information:
4	8	1
7	7	2
2	6	3
8	5	4
6	4	5
9	3	6
3	2	7
5	1	8
-------------------------------------------------------
Running...
##############################################
# #	CUP	Pri	W	T
# 4	8	1	28	36	
# 7	7	2	21	28	
# 2	6	3	15	21	
# 8	5	4	10	15	
# 6	4	5	6	10	
# 9	3	6	3	6	
# 3	2	7	1	3	
# 5	1	8	0	1	
##############################################
# Avg. Waiting Time:       :10.50
# Avg. Turnaround Time:    :15.00
##############################################
ying@ubuntu:~/project3/cs441-f16-project-3$ ./scheduler -s 3 tests/test3.txt
Scheduler     : 3 Priority
Scheduler     : 4 RR
Quantum       : 0
Sch.file      : tests/test3.txt
---------------------------------------------
Arrival Order: 
4, 7, 2, 8, 6, 9, 3, 5, 
Processing information:
4	8	1
7	7	2
2	6	3
8	5	4
6	4	5
9	3	6
3	2	7
5	1	8
-------------------------------------------------------
Arrival Order: 
4, 7, 2, 8, 6, 9, 3, 5, 
Processing information:
4	8	1
7	7	2
2	6	3
8	5	4
6	4	5
9	3	6
3	2	7
5	1	8
-------------------------------------------------------
Running...
##############################################
# #	CUP	Pri	W	T
# 4	8	1	0	8	
# 7	7	2	8	15	
# 2	6	3	15	21	
# 8	5	4	21	26	
# 6	4	5	26	30	
# 9	3	6	30	33	
# 3	2	7	33	35	
# 5	1	8	35	36	
##############################################
# Avg. Waiting Time:       :21.00
# Avg. Turnaround Time:    :25.50
##############################################
ying@ubuntu:~/project3/cs441-f16-project-3$ ./scheduler -s 4 tests/test3.txt
Round-Robin needs a quantum
ying@ubuntu:~/project3/cs441-f16-project-3$ ./scheduler -s 4 tests/test3.txt -q 3
Scheduler     : 4 RR
Quantum       : 3
Sch.file      : tests/test3.txt
---------------------------------------------
Arrival Order: 
4, 7, 2, 8, 6, 9, 3, 5, 
Processing information:
4	8	1
7	7	2
2	6	3
8	5	4
6	4	5
9	3	6
3	2	7
5	1	8
-------------------------------------------------------
Running...
##############################################
# #	CUP	Pri	W	T
# 4	8	1	27	35	
# 7	7	2	29	36	
# 2	6	3	24	30	
# 8	5	4	27	32	
# 6	4	5	29	33	
# 9	3	6	15	18	
# 3	2	7	18	20	
# 5	1	8	20	21	
##############################################
# Avg. Waiting Time:       :23.62
# Avg. Turnaround Time:    :28.12
##############################################
ying@ubuntu:~/project3/cs441-f16-project-3$ 

4. tests/text4.txt
Testing CUP burst and priority are both in ascend order. Testing four algorithm.
ying@ubuntu:~/project3/cs441-f16-project-3$ ./scheduler -s 1 tests/test4.txt
Scheduler     : 1 FCFS
Quantum       : 0
Sch.file      : tests/test4.txt
---------------------------------------------
Arrival Order: 
3, 6, 4, 8, 9, 
Processing information:
3	1	1
6	2	2
4	3	3
8	4	4
9	5	5
-------------------------------------------------------
Running...
##############################################
# #	CUP	Pri	W	T
# 3	1	1	0	1	
# 6	2	2	1	3	
# 4	3	3	3	6	
# 8	4	4	6	10	
# 9	5	5	10	15	
##############################################
# Avg. Waiting Time:       :4.00
# Avg. Turnaround Time:    :7.00
##############################################
ying@ubuntu:~/project3/cs441-f16-project-3$ ./scheduler -s 2 tests/test4.txt
Scheduler     : 2 SJF
Scheduler     : 3 Priority
Scheduler     : 4 RR
Quantum       : 0
Sch.file      : tests/test4.txt
---------------------------------------------
Arrival Order: 
3, 6, 4, 8, 9, 
Processing information:
3	1	1
6	2	2
4	3	3
8	4	4
9	5	5
-------------------------------------------------------
Arrival Order: 
3, 6, 4, 8, 9, 
Processing information:
3	1	1
6	2	2
4	3	3
8	4	4
9	5	5
-------------------------------------------------------
Running...
##############################################
# #	CUP	Pri	W	T
# 3	1	1	0	1	
# 6	2	2	1	3	
# 4	3	3	3	6	
# 8	4	4	6	10	
# 9	5	5	10	15	
##############################################
# Avg. Waiting Time:       :4.00
# Avg. Turnaround Time:    :7.00
##############################################
ying@ubuntu:~/project3/cs441-f16-project-3$ ./scheduler -s 3 tests/test4.txt
Scheduler     : 3 Priority
Scheduler     : 4 RR
Quantum       : 0
Sch.file      : tests/test4.txt
---------------------------------------------
Arrival Order: 
3, 6, 4, 8, 9, 
Processing information:
3	1	1
6	2	2
4	3	3
8	4	4
9	5	5
-------------------------------------------------------
Arrival Order: 
3, 6, 4, 8, 9, 
Processing information:
3	1	1
6	2	2
4	3	3
8	4	4
9	5	5
-------------------------------------------------------
Running...
##############################################
# #	CUP	Pri	W	T
# 3	1	1	0	1	
# 6	2	2	1	3	
# 4	3	3	3	6	
# 8	4	4	6	10	
# 9	5	5	10	15	
##############################################
# Avg. Waiting Time:       :4.00
# Avg. Turnaround Time:    :7.00
##############################################
ying@ubuntu:~/project3/cs441-f16-project-3$ ./scheduler -s 4 tests/test4.txt -q 2
Scheduler     : 4 RR
Quantum       : 2
Sch.file      : tests/test4.txt
---------------------------------------------
Arrival Order: 
3, 6, 4, 8, 9, 
Processing information:
3	1	1
6	2	2
4	3	3
8	4	4
9	5	5
-------------------------------------------------------
Running...
##############################################
# #	CUP	Pri	W	T
# 3	1	1	0	1	
# 6	2	2	1	3	
# 4	3	3	7	10	
# 8	4	4	8	12	
# 9	5	5	10	15	
##############################################
# Avg. Waiting Time:       :5.20
# Avg. Turnaround Time:    :8.20
##############################################
ying@ubuntu:~/project3/cs441-f16-project-3$ 

5. tests/test5.txt
Testing random number of CPU burst and priority.
ying@ubuntu:~/project3/cs441-f16-project-3$ ./scheduler -s 1 tests/test5.txt
Scheduler     : 1 FCFS
Quantum       : 0
Sch.file      : tests/test5.txt
---------------------------------------------
Arrival Order: 
2, 7, 3, 5, 8, 6, 1, 
Processing information:
2	4	5
7	3	6
3	5	2
5	7	1
8	2	6
6	4	3
1	3	7
-------------------------------------------------------
Running...
##############################################
# #	CUP	Pri	W	T
# 2	4	5	0	4	
# 7	3	6	4	7	
# 3	5	2	7	12	
# 5	7	1	12	19	
# 8	2	6	19	21	
# 6	4	3	21	25	
# 1	3	7	25	28	
##############################################
# Avg. Waiting Time:       :12.57
# Avg. Turnaround Time:    :16.57
##############################################
ying@ubuntu:~/project3/cs441-f16-project-3$ ./scheduler -s 2 tests/test5.txt
Scheduler     : 2 SJF
Scheduler     : 3 Priority
Scheduler     : 4 RR
Quantum       : 0
Sch.file      : tests/test5.txt
---------------------------------------------
Arrival Order: 
2, 7, 3, 5, 8, 6, 1, 
Processing information:
2	4	5
7	3	6
3	5	2
5	7	1
8	2	6
6	4	3
1	3	7
-------------------------------------------------------
Arrival Order: 
2, 7, 3, 5, 8, 6, 1, 
Processing information:
2	4	5
7	3	6
3	5	2
5	7	1
8	2	6
6	4	3
1	3	7
-------------------------------------------------------
Running...
##############################################
# #	CUP	Pri	W	T
# 2	4	5	8	12	
# 7	3	6	2	5	
# 3	5	2	16	21	
# 5	7	1	21	28	
# 8	2	6	0	2	
# 6	4	3	12	16	
# 1	3	7	5	8	
##############################################
# Avg. Waiting Time:       :9.14
# Avg. Turnaround Time:    :13.14
##############################################
ying@ubuntu:~/project3/cs441-f16-project-3$ ./scheduler -s 3 tests/test5.txt
Scheduler     : 3 Priority
Scheduler     : 4 RR
Quantum       : 0
Sch.file      : tests/test5.txt
---------------------------------------------
Arrival Order: 
2, 7, 3, 5, 8, 6, 1, 
Processing information:
2	4	5
7	3	6
3	5	2
5	7	1
8	2	6
6	4	3
1	3	7
-------------------------------------------------------
Arrival Order: 
2, 7, 3, 5, 8, 6, 1, 
Processing information:
2	4	5
7	3	6
3	5	2
5	7	1
8	2	6
6	4	3
1	3	7
-------------------------------------------------------
Running...
##############################################
# #	CUP	Pri	W	T
# 2	4	5	16	20	
# 7	3	6	20	23	
# 3	5	2	7	12	
# 5	7	1	0	7	
# 8	2	6	23	25	
# 6	4	3	12	16	
# 1	3	7	25	28	
##############################################
# Avg. Waiting Time:       :14.71
# Avg. Turnaround Time:    :18.71
##############################################
ying@ubuntu:~/project3/cs441-f16-project-3$ ./scheduler -s 4 tests/test5.txt -q 3
Scheduler     : 4 RR
Quantum       : 3
Sch.file      : tests/test5.txt
---------------------------------------------
Arrival Order: 
2, 7, 3, 5, 8, 6, 1, 
Processing information:
2	4	5
7	3	6
3	5	2
5	7	1
8	2	6
6	4	3
1	3	7
-------------------------------------------------------
Running...
##############################################
# #	CUP	Pri	W	T
# 2	4	5	17	21	
# 7	3	6	3	6	
# 3	5	2	18	23	
# 5	7	1	21	28	
# 8	2	6	12	14	
# 6	4	3	23	27	
# 1	3	7	17	20	
##############################################
# Avg. Waiting Time:       :15.86
# Avg. Turnaround Time:    :19.86
##############################################
ying@ubuntu:~/project3/cs441-f16-project-3$ 


## Test Suite

make check > output.txt
Pass all tests.
The result is in the output.txt

Test five test cases in tests/.


## Known bugs and problem areas

TODO
